/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.client

import java.net.URL
import java.util.ResourceBundle
import javafx.beans.binding.ObjectBinding
import javafx.beans.property._
import javafx.beans.value.{ ObservableStringValue, ObservableValue }
import javafx.fxml.{ FXML, Initializable }
import javafx.scene.control.TreeTableColumn.CellDataFeatures
import javafx.scene.control._
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane
import javafx.scene.paint.{ Color, Paint }
import javafx.scene.shape.Rectangle
import javafx.scene.text.{ Font, Text }
import javafx.util.Callback

import org.imcharsi.minesweepersolver.client.ResultWrapper._
import org.imcharsi.minesweepersolver.common.data.GameMap._
import org.imcharsi.minesweepersolver.common.data.Position2d.Position
import org.imcharsi.minesweepersolver.common.data.{ GameMap, Position2d }
import org.imcharsi.minesweepersolver.common.util.FP.{ State, StateEitherOptionMonad }
import org.imcharsi.minesweepersolver.common.util.{ Printer2d, Random }
import rx.lang.scala.schedulers.IOScheduler
import rx.lang.scala.{ JavaConversions => JC, Subject, Subscription }
import rx.observables.{ JavaFxObservable => JFO }
import rx.schedulers.JavaFxScheduler

import scala.collection.immutable.Queue
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.Try

/**
 * Created by Kang Woo, Lee on 9/27/16.
 */
class Client2dController extends Initializable {
  val javaFxScheduler = JC.javaSchedulerToScalaScheduler(JavaFxScheduler.getInstance())
  val ioScheduler = IOScheduler()
  val ioWorker = ioScheduler.createWorker
  implicit val seom = new StateEitherOptionMonad[GameState[Position], Unit]
  val edgeSize = 20

  @FXML
  private var scrollPane: ScrollPane = null
  @FXML
  private var scrollPaneContent: Pane = null
  @FXML
  private var textFieldWidth: TextField = null
  @FXML
  private var textFieldHeight: TextField = null
  @FXML
  private var textFieldMineCount: TextField = null
  @FXML
  private var textFieldSeed: TextField = null
  @FXML
  private var buttonStart: Button = null
  @FXML
  private var toggleButtonPause: ToggleButton = null
  @FXML
  private var buttonNextSeed: Button = null
  @FXML
  private var treeTableView: TreeTableView[ResultWrapper[Position]] = null
  @FXML
  private var treeTableColumnC1: TreeTableColumn[ResultWrapper[Position], String] = null
  @FXML
  private var treeTableColumnC2: TreeTableColumn[ResultWrapper[Position], String] = null
  @FXML
  private var treeTableColumnC3: TreeTableColumn[ResultWrapper[Position], String] = null

  // 이유를 모르겠다. 이와 같이 binding 을 만들면, textField 의 textProperty 가 바뀔 때마다 값이 바뀌지 않는다.
  // 그런데, SimpleStringProperty 를 만들어서 직접 값을 바꿔보면 동작한다.
  def intBinding(textProperty: ObservableStringValue): ObjectBinding[Option[Int]] =
    new ObjectBinding[Option[Int]] {
      bind(textProperty)

      override def onInvalidating(): Unit = {
        Console.out.println("inv")
        super.onInvalidating()
      }

      override def computeValue(): Option[Int] = {
        Try(Integer.valueOf(textProperty.get())).toOption.map(_.toInt)
      }
    }

  val rectangleMapProperty: ObjectProperty[Map[Position, Rectangle]] = new SimpleObjectProperty(Map())
  val subjectProperty: ObjectProperty[Option[(Subject[FSMOutput[Position]], Subscription)]] = new SimpleObjectProperty(None)
  val startProperty = new SimpleBooleanProperty(false)
  val runningProperty = new SimpleBooleanProperty(false)
  val pauseProperty = new SimpleBooleanProperty(false)
  val temporaryDisablePauseProperty = new SimpleBooleanProperty(false)

  def composedF(size: Position) =
    seom.orElse[FSMOutput[Position]](
      (_, b, _) => b
    )(
        State(GameMap.stepOn[Position](Position2d.allAroundPositionF, Position2d.inF(size)))
      )(
          seom.bind((_: FSMOutput[Position]) => State(GameMap.eliminateNotExposedNonZeroCleanCells(Position2d.allAroundPositionF, Position2d.inF(size))))
            .andThen(
              seom.bind((_: FSMOutput[Position]) => State(GameMap.computeConnectedPositionSequence(Position2d.allAdjacentPositionF)))
            )
            .andThen(
              seom.bind(x => State(GameMap.computeAdjacentPositionPowerSet(Position2d.assertAdjacent)(x)))
            )
            .andThen(
              seom.bind(x => State(GameMap.tryToSolveThenPushToBeSteppedOn(Position2d.allAroundPositionF, Position2d.inF(size), Implicits.global)(x)))
            )(seom.unit(NothingToDo()))
        )

  override def initialize(location: URL, resources: ResourceBundle): Unit = {
    val scrollPaneVal = scrollPane
    val scrollPaneContentVal = scrollPaneContent
    val textFieldWidthVal = textFieldWidth
    val textFieldHeightVal = textFieldHeight
    val textFieldMineCountVal = textFieldMineCount
    val textFieldSeedVal = textFieldSeed
    val buttonStartVal = buttonStart
    val toggleButtonPauseVal = toggleButtonPause
    val buttonNextSeedVal = buttonNextSeed
    val treeTableViewVal = treeTableView
    val treeTableColumnC1Val = treeTableColumnC1
    val treeTableColumnC2Val = treeTableColumnC2
    val treeTableColumnC3Val = treeTableColumnC3

    val treeItemRoot = new TreeItem[ResultWrapper[Position]](RootWrapper())
    treeTableViewVal.rootProperty().set(treeItemRoot)
    treeTableColumnC1Val
      .cellValueFactoryProperty()
      .set(
        new Callback[CellDataFeatures[ResultWrapper[Position], String], ObservableValue[String]] {
          override def call(param: CellDataFeatures[ResultWrapper[Position], String]): ObservableValue[String] =
            param.getValue.getValue match {
              case RootWrapper() =>
                new ReadOnlyStringWrapper("")
              case SteppedOnLevel1(p, _) =>
                new ReadOnlyStringWrapper(p.toString)
              case FoundAnswerLevel1() =>
                new ReadOnlyStringWrapper("FoundAnswer")
              case NotFoundAnswerLevel1() =>
                new ReadOnlyStringWrapper("NotFoundAnswer")
              case _ =>
                new ReadOnlyStringWrapper("")
            }
        }
      )
    treeTableColumnC2Val
      .cellValueFactoryProperty()
      .set(
        new Callback[CellDataFeatures[ResultWrapper[Position], String], ObservableValue[String]] {
          override def call(param: CellDataFeatures[ResultWrapper[Position], String]): ObservableValue[String] =
            param.getValue.getValue match {
              case FoundAnswerLevel2(ps, _, _) =>
                new ReadOnlyStringWrapper(ps.mkString(","))
              case NotFoundAnswerLevel2(ps, _, _) =>
                new ReadOnlyStringWrapper(ps.mkString(","))
              case _ =>
                new ReadOnlyStringWrapper("")
            }
        }
      )
    treeTableColumnC3Val
      .cellValueFactoryProperty()
      .set(
        new Callback[CellDataFeatures[ResultWrapper[Position], String], ObservableValue[String]] {
          override def call(param: CellDataFeatures[ResultWrapper[Position], String]): ObservableValue[String] =
            param.getValue.getValue match {
              case FoundAnswerLevel3(position, answer, _) =>
                new ReadOnlyStringWrapper(s"${position} ${answer}")
              case NotFoundAnswerLevel3(position, answer, _) =>
                new ReadOnlyStringWrapper(s"${position} ${answer}")
              case _ =>
                new ReadOnlyStringWrapper("")
            }
        }
      )

    treeTableColumnC2Val.prefWidthProperty().bind(treeTableViewVal.widthProperty().subtract(treeTableColumnC1Val.widthProperty()).divide(2))
    treeTableColumnC3Val.prefWidthProperty().bind(treeTableViewVal.widthProperty().subtract(treeTableColumnC1Val.widthProperty()).subtract(treeTableColumnC2Val.widthProperty()))

    toggleButtonPauseVal.disableProperty().set(true)
    buttonStartVal.disableProperty().bind(runningProperty.and(toggleButtonPauseVal.selectedProperty().not()))
    buttonNextSeedVal.disableProperty().bind(runningProperty.and(toggleButtonPauseVal.selectedProperty().not()))
    toggleButtonPauseVal.disableProperty().bind(buttonStartVal.disableProperty().not.and(startProperty.not()).or(temporaryDisablePauseProperty))
    scrollPaneContentVal.disableProperty().bind(runningProperty.or(startProperty.not()))
    treeTableViewVal.disableProperty().bind(runningProperty)

    RxUtil
      .combine(
        JC.toScalaObservable(JFO.fromActionEvents(buttonStartVal)),
        JC.toScalaObservable(JFO.fromObservableValue(textFieldWidthVal.textProperty()))
          .map(s => Try(Integer.valueOf(s)).toOption.map(_.toInt))
          .filter(_.isDefined)
          .combineLatest(
            JC.toScalaObservable(JFO.fromObservableValue(textFieldHeightVal.textProperty()))
              .map(s => Try(Integer.valueOf(s)).toOption.map(_.toInt))
              .filter(_.isDefined)
          )
          .combineLatest(
            JC.toScalaObservable(JFO.fromObservableValue(textFieldMineCountVal.textProperty()))
              .map(s => Try(Integer.valueOf(s)).toOption.map(_.toInt))
              .filter(_.isDefined)
          )
          .combineLatest(
            JC.toScalaObservable(JFO.fromObservableValue(textFieldSeedVal.textProperty()))
              .map(s => Try(Integer.valueOf(s)).toOption.map(_.toInt))
              .filter(_.isDefined)
          )
          .combineLatest(
            JC.toScalaObservable(JFO.fromObservableValue(subjectProperty))
          )
      )
      .observeOn(ioScheduler)
      .filter {
        case (_, Some(((((Some(_), Some(_)), Some(_)), Some(_)), _))) => true
        case _ => false
      }
      .map { case (_, Some(((((Some(width), Some(height)), Some(mineCount)), Some(seed)), subjectSubscription))) => (width, height, mineCount, seed, subjectSubscription) }
      .map {
        case (width, height, mineCount, seed, subjectSubscription) =>
          Tuple2(
            (width, height, mineCount, seed, subjectSubscription),
            0.until(width).foldLeft(Map[Position, Rectangle]()) { (map, x) =>
              0.until(height).foldLeft(map) { (map, y) =>
                val rectangle = new Rectangle(edgeSize, edgeSize, Color.GREEN)
                rectangle.strokeProperty().set(Color.BLACK)
                rectangle.xProperty().set(x * edgeSize)
                rectangle.yProperty().set(y * edgeSize)
                map + (Position(x, y) -> rectangle)
              }
            }
          )
      }
      .subscribe { x =>
        x match {
          case ((width, height, mineCount, seed, subjectSubscription), rectangleMap) =>
            subjectSubscription.foreach(_._2.unsubscribe())
            val size: Position = Position(width, height)
            val beSteppedOnPositions = Set(Position(0, 0), size.addX(-1).addY(-1), size.addX(-1).yTo(0), size.addY(-1).xTo(0))
            val (mineMap, _) = Random.putMineCell(Random.random)(size, mineCount, seed, beSteppedOnPositions)
            Console.out.println(Printer2d.mapToString(size, mineMap))
            val gameState =
              GameMap.GameState(
                GameMap.GameMap(
                  size,
                  GameMap.arrangeMap[Position](
                    Position2d.allAroundPositionF,
                    Position2d.inF(size),
                    Position2d.origin
                  )(size, mineMap)
                ),
                Queue(beSteppedOnPositions.toList: _*),
                beSteppedOnPositions,
                Set()
              )
            val initFsmOutput: Either[Unit, Option[FSMOutput[Position]]] = Right(Some(NothingToDo()))
            val w = composedF(size)
            val subject = Subject[FSMOutput[Position]]()

            val subscription =
              subject
                .combineLatest(
                  JC.toScalaObservable(JFO.fromObservableValue(toggleButtonPauseVal.selectedProperty()))
                )
                .scan((gameState, (initFsmOutput, true))) {
                  case ((gameState, _), (recentFsmOutput, selected)) =>
                    val modifiedGameState =
                      recentFsmOutput match {
                        case NothingToDo() => gameState
                        case ManualStepOn(p) if !gameState.beSteppedOnCellLookup.contains(p) =>
                          gameState.copy(
                            beSteppedOnCell = gameState.beSteppedOnCell.enqueue(p),
                            beSteppedOnCellLookup = gameState.beSteppedOnCellLookup + p
                          )
                        case ManualStepOn(_) => gameState
                      }
                    val (newGameState, newFSMOutput) = w.f(modifiedGameState)
                    (newGameState, (newFSMOutput, selected))
                }
                .observeOn(javaFxScheduler)
                .subscribe(
                  x => {
                    ioWorker.schedule {
                      Console.out.println(Printer2d.mapToString(size, x._1.gameMap.map))
                      Console.out.println(x._2)
                      Console.out.println(x._1.beSteppedOnCell)
                      Console.out.println(x._1.beSteppedOnCellLookup)
                    }
                    x._2 match {
                      case (Right(Some(NothingToDo())), _) =>
                        scrollPaneContentVal.getChildren.clear()
                        scrollPaneContentVal.getChildren.addAll(rectangleMap.values.toList: _*)
                        rectangleMapProperty.set(rectangleMap)
                        scrollPaneContentVal.prefWidthProperty().set(width * edgeSize)
                        scrollPaneContentVal.prefHeightProperty().set(height * edgeSize)
                        runningProperty.set(true)
                        startProperty.set(true)
                        temporaryDisablePauseProperty.set(false)
                        toggleButtonPauseVal.selectedProperty().set(false)
                        treeItemRoot.getChildren.clear()
                        ioWorker.schedule(0.05.seconds)(subject.onNext(NothingToDo()))
                      case (Right(Some(fsmOutput @ NotFoundAnswer(invalidAnswers))), _) =>
                        runningProperty.set(false)
                        // 답을 찾지 못해서 멈춘 경우에 일시정지 기능을 활성화시켜두면 이상하다.
                        temporaryDisablePauseProperty.set(true)

                        val treeItemLevel1 = new TreeItem[ResultWrapper[Position]](NotFoundAnswerLevel1())
                        invalidAnswers.toStream.flatMap(_.flatMap(_.toStream))
                          .foreach {
                            case (ps, map) =>
                              val treeItemLevel2 = new TreeItem[ResultWrapper[Position]](NotFoundAnswerLevel2(ps, map, x._1.gameMap.map))
                              map.foreach {
                                case (position, answer) =>
                                  treeItemLevel2.getChildren.add(
                                    new TreeItem[ResultWrapper[Position]](NotFoundAnswerLevel3(position, answer, x._1.gameMap.map))
                                  )
                              }
                              treeItemLevel1.getChildren.add(treeItemLevel2)
                          }
                        treeItemRoot.getChildren.add(treeItemLevel1)
                      case (Right(Some(fsmOutput @ FoundAnswer(answerList))), selected) =>
                        answerList.map(_._2).foreach { answerMap =>
                          answerMap.toList.foreach {
                            case (position, true) =>
                              rectangleMap.get(position).foreach(_.fillProperty().set(Color.RED))
                            case (position, false) =>
                              Console.out.println(position)
                              x._1.gameMap.map.get(position).foreach {
                                case CleanCell(aroundMineCount) =>
                                  rectangleMap.get(position).foreach { rectangle =>
                                    rectangle.fillProperty().set(Color.YELLOW)
                                    // http://www.developer.com/java/data/using-graphics-in-javafx.html
                                    val text = new Text(position.x * edgeSize, (position.y + 1) * edgeSize, aroundMineCount.toString)
                                    text.fontProperty().set(new Font(edgeSize))
                                    scrollPaneContentVal.getChildren.add(text)
                                  }
                                // 이 예는 필요하다. 왜냐하면, 이 경우는 계산에 의해 지뢰가 없는 칸으로 판단되었고
                                // 그래서 밟을 예정이기 때문이다. 결론은, 지뢰가 없는 칸이라는 판단에 대해서는 아무것도 하지 않는다.
                                case UnknownCell(_) =>
                              }
                          }
                        }
                        val treeItemLevel1 = new TreeItem[ResultWrapper[Position]](FoundAnswerLevel1())
                        answerList
                          .foreach {
                            case (ps, map) =>
                              val treeItemLevel2 = new TreeItem[ResultWrapper[Position]](FoundAnswerLevel2(ps, map, x._1.gameMap.map))
                              map.foreach {
                                case (position, answer) =>
                                  treeItemLevel2.getChildren.add(
                                    new TreeItem[ResultWrapper[Position]](FoundAnswerLevel3(position, answer, x._1.gameMap.map))
                                  )
                              }
                              treeItemLevel1.getChildren.add(treeItemLevel2)
                          }
                        treeItemRoot.getChildren.add(treeItemLevel1)
                        treeTableViewVal.getSelectionModel.clearSelection()

                        runningProperty.set(!selected)
                        if (!selected)
                          ioWorker.schedule(0.05.seconds)(subject.onNext(NothingToDo()))
                      case (Right(Some(fsmOutput @ SteppedOn(p))), selected) =>
                        x._1.gameMap.map.get(p).foreach {
                          case CleanCell(aroundMineCount) =>
                            rectangleMap.get(p).foreach { rectangle =>
                              rectangle.fillProperty().set(Color.YELLOW)
                              val text = new Text(p.x * edgeSize, (p.y + 1) * edgeSize, aroundMineCount.toString)
                              text.fontProperty().set(new Font(edgeSize))
                              scrollPaneContentVal.getChildren.add(text)
                            }
                        }
                        runningProperty.set(!selected)
                        if (!selected)
                          ioWorker.schedule(0.05.seconds)(subject.onNext(NothingToDo()))
                        val treeItemLevel1 = new TreeItem[ResultWrapper[Position]](SteppedOnLevel1(p, x._1.gameMap.map))
                        treeTableViewVal.getSelectionModel.clearSelection()
                        treeItemRoot.getChildren.add(treeItemLevel1)
                      case x =>
                        Console.out.println(x)
                        runningProperty.set(false)
                        startProperty.set(false)
                    }
                  }
                )
            subjectProperty.set(Some(subject, subscription))
        }
      }

    RxUtil
      .combine(
        JC.toScalaObservable(JFO.fromActionEvents(buttonNextSeed)),
        JC.toScalaObservable(JFO.fromObservableValue(textFieldSeedVal.textProperty()))
          .map(s => Try(Integer.valueOf(s)).toOption.map(_.toInt))
          .filter(_.isDefined)
      )
      .filter {
        case (_, Some(Some(_))) => true
        case _ => false
      }
      .map {
        case (_, Some(Some(seed))) => seed
      }
      .observeOn(javaFxScheduler)
      .subscribe(
        seed => {
          textFieldSeedVal.textProperty().set(Random.random(seed).toString)
        }
      )
    // 각각의 사각형에 대해 입력을 받는 방법도 있지만, 큰 판에 대해 입력을 받아서 좌표를 계산하기로 하기.
    RxUtil
      .combine(
        JC.toScalaObservable(JFO.fromNodeEvents(scrollPaneContentVal, MouseEvent.MOUSE_CLICKED)),
        JC.toScalaObservable(JFO.fromObservableValue(rectangleMapProperty))
          .combineLatest(JC.toScalaObservable(JFO.fromObservableValue(subjectProperty)))
      )
      .observeOn(javaFxScheduler)
      .subscribe { x =>
        x match {
          case (mouseEvent, Some((map, Some((subject, _))))) =>
            val x = (mouseEvent.getX / edgeSize).toInt
            val y = (mouseEvent.getY / edgeSize).toInt
            runningProperty.set(true)
            // 답을 찾지 못해서 멈췄다가, 다시 수동입력을 한 경우 일시정지 기능을 활성화시킨다.
            temporaryDisablePauseProperty.set(false)
            treeTableViewVal.getSelectionModel.clearSelection()
            subject.onNext(ManualStepOn(Position(x, y)))
          case _ =>
        }
      }

    def allocRectangle(p: Position, f: Paint): Rectangle = {
      val rectangle = new Rectangle(edgeSize, edgeSize, Color.TRANSPARENT)
      rectangle.strokeProperty().set(f)
      rectangle.strokeWidthProperty().set(5)
      rectangle.xProperty().set(p.x * edgeSize)
      rectangle.yProperty().set(p.y * edgeSize)
      rectangle
    }
    // fromObservableValueChanges 를 쓰지 않은 이유는, TreeTableView.clearSelection() 을 할 때 구독이 풀렸기 때문이다.
    JC.toScalaObservable(JFO.fromObservableValue(treeTableViewVal.getSelectionModel.selectedItemProperty()))
      .observeOn(ioScheduler)
      .map(x => Option(x))
      .scan[(List[Rectangle], List[Rectangle], List[Rectangle])]((List(), List(), List())) {
        case ((x, _, _), treeItem) =>
          val beRemoved = x
          val newRectangle =
            treeItem.map(_.getValue) match {
              case Some(SteppedOnLevel1(p, _)) => List(allocRectangle(p, Color.LIGHTBLUE))
              case Some(FoundAnswerLevel2(ps, map, _)) =>
                ps.map(p => allocRectangle(p, Color.RED)).toList ++
                  map.toList.map {
                    case (p, true) => allocRectangle(p, Color.DARKBLUE)
                    case (p, false) => allocRectangle(p, Color.LIGHTBLUE)
                  }
              case Some(FoundAnswerLevel3(position, b, _)) =>
                if (b)
                  List(allocRectangle(position, Color.DARKBLUE))
                else
                  List(allocRectangle(position, Color.LIGHTBLUE))
              case Some(NotFoundAnswerLevel2(ps, map, _)) =>
                ps.map(p => allocRectangle(p, Color.RED)).toList ++
                  map.toList.map {
                    case (p, true) => allocRectangle(p, Color.DARKBLUE)
                    case (p, false) => allocRectangle(p, Color.LIGHTBLUE)
                  }
              case Some(NotFoundAnswerLevel3(position, b, _)) =>
                if (b)
                  List(allocRectangle(position, Color.DARKBLUE))
                else
                  List(allocRectangle(position, Color.LIGHTBLUE))
              case _ => List()
            }
          (newRectangle, beRemoved, newRectangle)
      }
      .observeOn(javaFxScheduler)
      .subscribe(
        x => {
          scrollPaneContentVal.getChildren.removeAll(x._2: _*)
          scrollPaneContentVal.getChildren.addAll(x._3: _*)
        }
      )
  }
}
