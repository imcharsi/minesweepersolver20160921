/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.client

import rx.lang.scala.{ Subscription, Observer, Observable }

/**
 * Created by Kang Woo, Lee on 9/27/16.
 */
object RxUtil {
  def combine[A, B](observableA: Observable[A], observableB: Observable[B]): Observable[(A, Option[B])] = {
    def observerF(observer: Observer[(A, Option[B])]): Subscription = {
      var optionB: Option[B] = None
      def onNextB(b: B): Unit = optionB = Some(b)
      val subscriptionB = observableB.subscribe(onNextB, observer.onError, observer.onCompleted)
      def onNextA(a: A): Unit = observer.onNext((a, optionB))
      val subscriptionA = observableA.subscribe(onNextA, observer.onError, observer.onCompleted)
      Subscription {
        subscriptionA.unsubscribe()
        subscriptionB.unsubscribe()
      }
    }
    Observable.create(observerF)
  }
}
