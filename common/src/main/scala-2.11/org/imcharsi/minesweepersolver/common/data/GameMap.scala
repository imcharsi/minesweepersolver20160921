/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.data

import org.imcharsi.minesweepersolver.common.combinator.Combinator

import scala.collection.immutable.Queue
import scala.concurrent.duration.Duration
import scala.concurrent.{ ExecutionContext, Await, Future }

/**
 * Created by Kang Woo, Lee on 9/21/16.
 */
object GameMap {

  sealed trait GameCell

  case class UnknownCell(origin: GameCell) extends GameCell

  case class CleanCell(aroundMineCellCount: Int) extends GameCell

  case object MineCell extends GameCell

  // 원래 칸은, 지뢰가 없는 칸일수도 있다.
  case class ExpectedAsMineCell(origin: GameCell) extends GameCell

  sealed case class GameMap[P](size: P, map: Map[P, GameCell])

  // 여러 개의 상태를 두는 것이 필요한지 생각해보기.
  // 일단은, 여러 개의 State 함수들이 각각 필요한 것을 시도해서 하나라도 성공하면, 이후의 시도는 전부 건너뛰도록 구상했다.
  case class GameState[P](
    gameMap: GameMap[P],
    // 순서를 맞추는 용도로 쓴다.
    beSteppedOnCell: Queue[P],
    // 찾기를 빨리 하는 용도로 쓴다.
    beSteppedOnCellLookup: Set[P],
    // nonZeroCleanCells 에서 좌표를 지우는 조건은, 해당 좌표 주위로 UnknownCell 이 없을 때이다.
    // 더 이상 beSteppedOnCell 을 채워넣지 못할 때, 일괄 정리한다.
    nonZeroCleanCells: Set[P]
  )

  // 이 기능은, 이미 밟았고 주위에 지뢰가 있는 것으로 판단된 칸들 중, 이어지는 칸들을 전부 찾는 용도로 쓴다.
  // 아직 구상단계라, 이어지는 칸들의 집합이 여러개가 될 수 있는 상황은 생각하지 않았다.
  // 인자 isConnectedF 의 뜻이 햇갈릴 수 있는데, Map[P,GameCell]=>P=>Boolean 과 같은 모양에서 partial apply 된 결과이다.
  // 여기서는, 수평/수직 방향으로 이웃한 것만 다룬다. 대각선 방향으로 이웃한 것은 다루지 않는다.
  def findAllConnectedNext[P](isConnectedF: P => Boolean, nextPositionF: P => Stream[P])(beTestedQueue: Queue[P]): Set[P] = {
    def innerF(result: Set[P])(beTestedQueue: Queue[P]): Set[P] = {
      beTestedQueue.dequeueOption match {
        case Some((position, newRemaining)) =>
          if (isConnectedF(position))
            innerF(result + position)(
              nextPositionF(position)
                .foldLeft(newRemaining) {
                  case (q, p) =>
                    if (result.contains(p)) q else q.enqueue(p)
                }
            )
          else innerF(result)(newRemaining)
        case None =>
          result
      }
    }
    innerF(Set())(beTestedQueue)
  }

  sealed trait FSMOutput[P]

  case class NothingToDo[P]() extends FSMOutput[P]

  case class SteppedOn[P](p: P) extends FSMOutput[P]

  case class ConnectedPositionSequence[P](pss: Stream[Set[P]]) extends FSMOutput[P]

  case class AdjacentPositionPowerSet[P](ps: Stream[Stream[Stream[P]]]) extends FSMOutput[P]

  case class FoundAnswer[P](one: Stream[(Stream[P], Map[P, Boolean])]) extends FSMOutput[P]

  // 한 개의 답을 내지 못했을 때의 출력이다.
  case class NotFoundAnswer[P](several: Stream[Stream[Stream[(Stream[P], Map[P, Boolean])]]]) extends FSMOutput[P]

  // 수동으로 좌표를 찍을 때 쓴다.
  case class ManualStepOn[P](p: P) extends FSMOutput[P]

  sealed trait FSMInput[P]

  // 이 기능은, 지뢰만 그려진 map 을 바탕으로, 나머지에는 전부 CleanCell 을 계산해서 넣고 다시 UnknownCell 로 포장한다.
  // inF 는, 주어진 좌표 P 가 origin 은 포함하고, size 는 포함하지 않는 범위 안에 있는지 판단한다.
  def arrangeMap[P](nextPositionF: P => Stream[P], inF: P => Boolean, origin: P)(size: P, map: Map[P, GameCell]): Map[P, GameCell] = {
    // 모든 좌표를 만든다.
    def generateAllP(result: Set[P], changed: Boolean)(pQueue: Queue[P]): Set[P] = {
      pQueue.dequeueOption match {
        case Some((p, remainingPQueue)) =>
          val (newResult, newPQueue) =
            nextPositionF(p)
              .filter(inF(_))
              .foldLeft((result, remainingPQueue)) {
                case ((result, pQueue), p) =>
                  (result + p, if (!result.contains(p)) pQueue.enqueue(p) else pQueue)
              }
          generateAllP(newResult, false)(newPQueue)
        case _ => result
      }
    }

    val generatedAllP = generateAllP(Set[P](), false)(Queue(origin))
    val countedMap: Map[P, GameCell] =
      generatedAllP
        // 일단, 모든 좌표를 CleanCell(0) 로 채운다.
        .foldLeft(generatedAllP.toList.map(p => (p, CleanCell(0))).toMap) {
          case (resultMap, p) =>
            map.get(p) match {
              case Some(MineCell) =>
                // map 의 현재 좌표에 지뢰가 있으면, 현재 좌표의 근처에 있는 모든 cell 의 지뢰수를 바꾼다.
                nextPositionF(p)
                  .filter(inF(_))
                  .foldLeft(resultMap) {
                    case (resultMap, p) =>
                      val cell = resultMap(p)
                      resultMap + (p -> cell.copy(aroundMineCellCount = cell.aroundMineCellCount + 1))
                  }
              case None =>
                resultMap
            }
        }
    map.toList
      // 지뢰를 결과 map 에 덮어 쓴다.
      .foldLeft(countedMap) { case (countedMap, (p, cell)) => countedMap + (p -> cell) }
      // 이 단계에서 결과 map 의 cell 은 CleanCell 이거나 MineCell 인데 전부 UnknownCell 안에 넣는다.
      .map { case (p, cell) => (p, UnknownCell(cell)) }.toMap
  }

  // 아래 기능이 햇갈릴 수 있다.
  // 지뢰가 있다고 예상한 칸을 기준으로 주위의 칸을 고른 다음,
  // 이 칸들 중에서, 각 칸의 주위에 있는 지뢰의 개수와 실제로 지뢰가 있는 것으로 예상한 칸의 수가 같은 칸을 고른 후
  // 이 칸의 주위에 있는 UnknownCell 을 찾는다.
  def findUnknownCellAroundCompleted[P](nextPositionF: P => Stream[P], inF: P => Boolean)(beSteppedOnPosition: P, beSteppedOnCell: Queue[P], beSteppedOnCellLookup: Set[P], map: Map[P, GameCell]): (Queue[P], Set[P]) = {
    nextPositionF(beSteppedOnPosition)
      .filter(determineCompleted(nextPositionF, inF)(map))
      .flatMap(nextPositionF)
      .filter(inF)
      .map(p => map.get(p).map(cell => (p, cell)))
      .foldLeft((beSteppedOnCell, beSteppedOnCellLookup)) {
        case ((q, s), Some((p, UnknownCell(_)))) if !s.contains(p) =>
          (q.enqueue(p), s + p)
        case ((q, s), _) =>
          (q, s)
      }
  }

  // 현재 상태를 보고, 밟을 예정인 칸이 있으면 밟고, 없으면 아무것도 하지 않는다.
  def stepOn[P](nextPositionF: P => Stream[P], inF: P => Boolean)(state: GameState[P]): (GameState[P], Either[Unit, Option[FSMOutput[P]]]) = {
    state.beSteppedOnCell.dequeueOption match {
      case Some((beSteppedOnPosition, remainingBeSteppedOnCell)) =>
        // 주위에 지뢰가 없는 것으로 판단된 칸을 실제로 밟고, 밟은 칸 주위의 모든 칸을 밟을 예정인 칸 목록에 넣는다.
        // 밟을 예정인 칸을 하나라도 찾았으면, 이번 단계에서 필요한 처리를 했다는 표시를 한다.
        // 이 표시로, 이 단계에 이어서 합성될 다른 단계들을 생략할 수 있다.
        state.gameMap.map.get(beSteppedOnPosition) match {
          case Some(UnknownCell(origin @ CleanCell(0))) =>
            val (newBeSteppedOnCell, newBeSteppedOnCellLookup, changed) =
              nextPositionF(beSteppedOnPosition)
                // 여기서 좌표를 거르는 방법과 아래에서 None 경우도 받는 방법이 있는데, 좌표를 거르는 방법으로 했다.
                // impo 예상되지 않은 입력은 있을 수 없도록 엄격하게 해보기.
                .filter(inF)
                .foldLeft((remainingBeSteppedOnCell, state.beSteppedOnCellLookup, false)) {
                  case ((beSteppedOnCell, beSteppedOnCellLookup, changed), nextPosition) =>
                    state.gameMap.map.get(nextPosition) match {
                      // 아직 밟지 않았지만, 이미 밟을 예정인 칸일 수 있다.
                      // 밟을 예정인 칸이 중복될 수 있도록 하는 방법도 있는데, 중복되지 않는 방법으로 했다.
                      case Some(UnknownCell(_)) if !beSteppedOnCellLookup.contains(nextPosition) =>
                        (beSteppedOnCell.enqueue(nextPosition), beSteppedOnCellLookup + nextPosition, true)
                      case Some(_) =>
                        (beSteppedOnCell, beSteppedOnCellLookup, changed)
                    }
                }

            val newMap = state.gameMap.map + (beSteppedOnPosition -> origin)
            // 밟고 난 뒤에 항상, 주위의 완료된 칸을 찾아서, 완료된 칸 주위의 밟지 않은 칸을 밟을 예정 칸으로 해야 한다.
            val (newQueue, newSet) =
              findUnknownCellAroundCompleted(nextPositionF, inF)(
                beSteppedOnPosition,
                newBeSteppedOnCell,
                newBeSteppedOnCellLookup,
                newMap
              )

            Tuple2(
              state.copy(
                beSteppedOnCell = newQueue,
                beSteppedOnCellLookup = newSet - beSteppedOnPosition,
                gameMap =
                  state.gameMap.copy(map = newMap)
              ),
              // 이 단계에서 밟았으면 밟았다는 표시를 한다. 이 단계에서 밟을 예정인 칸을 찾았는지를 구분하지 안는다.
              Right(Some(SteppedOn(beSteppedOnPosition)))
            )
          case Some(UnknownCell(origin @ CleanCell(_))) =>
            val newMap = state.gameMap.map + (beSteppedOnPosition -> origin)
            val (newQueue, newSet) =
              findUnknownCellAroundCompleted(nextPositionF, inF)(
                beSteppedOnPosition,
                remainingBeSteppedOnCell,
                state.beSteppedOnCellLookup,
                newMap
              )

            Tuple2(
              state.copy(
                beSteppedOnCell = newQueue,
                beSteppedOnCellLookup = newSet - beSteppedOnPosition,
                gameMap =
                  state.gameMap.copy(map = newMap),
                nonZeroCleanCells = state.nonZeroCleanCells + beSteppedOnPosition
              ),
              Right(Some(SteppedOn(beSteppedOnPosition)))
            )
          // 지뢰가 없는 아직 밟지 않은 칸이라 판단했는데,
          // 지뢰가 있는 칸이었거나, 이미 밟은 칸이었다면 계산 실패.
          case _ => Tuple2(state, Left(()))
        }
      case None =>
        // 더 이상 밟을 칸이 없으면 아무것도 하지 않은 것이다.
        (state, Right(None))
    }
  }

  def eliminateNotExposedNonZeroCleanCells[P](nextPositionF: P => Stream[P], inF: P => Boolean)(state: GameState[P]): (GameState[P], Either[Unit, Option[FSMOutput[P]]]) = {
    def determineNotExposedF(p: P): Boolean = {
      nextPositionF(p).
        filter(inF)
        .forall { p =>
          state.gameMap.map.get(p) match {
            case Some(UnknownCell(_)) => false
            case Some(_) => true
            // None 은 있으면 안 된다. 예외가 생기게 놔둔다.
          }
        }
    }
    Tuple2(
      state
        .copy(
          nonZeroCleanCells =
            state.nonZeroCleanCells.filterNot(determineNotExposedF(_))
        ),
      Right(Some(NothingToDo()))
    )
  }

  def computeConnectedPositionSequence[P](nextPositionF: P => Stream[P])(state: GameState[P]): (GameState[P], Either[Unit, Option[FSMOutput[P]]]) = {
    def innerF(result: Stream[Set[P]])(remainNonZeroCleanCells: Set[P]): Stream[Set[P]] = {
      if (remainNonZeroCleanCells.isEmpty) {
        result
      } else {
        val x =
          findAllConnectedNext[P](
            state.nonZeroCleanCells.contains(_),
            nextPositionF
          )(Queue(remainNonZeroCleanCells.head))
        innerF(x #:: result)(remainNonZeroCleanCells.diff(x))
      }
    }
    (state, Right(Some(ConnectedPositionSequence(innerF(Stream.Empty)(state.nonZeroCleanCells)))))
  }

  def computeAdjacentPositionPowerSet[P](predF: (P, P) => Boolean)(connectedPositionSequence: FSMOutput[P])(state: GameState[P]): (GameState[P], Either[Unit, Option[FSMOutput[P]]]) = {
    connectedPositionSequence match {
      case ConnectedPositionSequence(pss) if !pss.isEmpty =>
        Tuple2(
          state,
          Right(
            Some(
              AdjacentPositionPowerSet(
                pss.map(ps => Combinator.adjacentPowerSet(predF)(ps.toStream))
              )
            )
          )
        )
      case ConnectedPositionSequence(pss) if pss.isEmpty =>
        Tuple2(state, Right(None))
      case _ =>
        Tuple2(state, Left(()))
    }
  }

  // 상당히 햇갈릴 수 있다. 이 구절이 이번 연습의 핵심이다. 이와 같이 풀기 위해서 조합연습을 했다.
  // 각 문장을 설명하기에 앞서 전체적인 구상을 가장 쉬운 예부터 들어서 설명해야 동작방식을 이해할 수 있다.
  // 먼저
  // 1 1 ?
  // ? ? ? 의 예를 들어 보자. 이 함수의 remaining 인자는 좌표열의 각 조합인데, 주어진 조합이 {1, 1} 이라 하자.
  // 문제는 아래와 같이 푼다. 먼저, 첫번째 1 에서 깃발을 꽂을 수 있는 조합을 찾는다.
  // t f
  // f t 가 된다.
  // 두번째 1 을 계산할 때, 앞단계의 조합계산결과와 지금 현재의 빈칸 중 겹치는 칸을 먼저 구하여 조합틀에 미리 써넣는다.
  // 이 경우 앞단계에서 구한 모든 계산결과와 겹치게 된다. 이어서 현재위치의 지뢰 수를 구한 뒤 여기서 현재 조합틀에 꽂아 놓은 깃발개수를 뺀다.
  // 이 예에서는 0 개가 된다.
  // 이어서 조합틀에 미리 써넣은 계산결과를 제외한 빈칸 수에서 조합후보로 사용할 깃발 개수를 뺀 수를, 조합후보로 사용할 빈칸 수로 한다.
  // 이 예에서 조합틀 {t f ? ?}, {f t ? ?} 이므로 빈칸수는 2개이고, 조합후보로 사용할 깃발 개수가 0개이므로, 조합후보로 사용할 빈 칸 수는 2개이다.
  // 그래서 {t f ? ?}, {f t ? ?} 조합 틀에 대해, {f, f} 조합후보로 조합을 계산하게 되어
  // {t f f f},{t f f f},{f t f f},{f t f f} 가 되고, 중복을 지워도 결국 답이 한개가 아니어서 문제를 풀지 못하게 된다.
  // 다른 예로서
  // 1 2 2 1
  // ? ? ? ? 를 조합열 {1, 2, 2, 1} 로 풀어보자. 앞의 설명대로 한 단계씩 반복해보면
  // 첫번째 1 에서 {t f}, {f t} 가 되고
  // 두번째 2 에서 {t f t}, {f t t} 가 되고
  // 세번째 2 에서 {t f t t}, {f t t f} 가 되고
  // 네번째 1 에서 {f t t f} 만 남는다.
  // 세번째 단계의 출력 중 첫번째를 네번째 단계에서 처리할 때,
  // 네번째 1 주위에 꽂았던 깃발 개수가 네번째 1 주위에 있는 지뢰 수보다 많아서 실패하게 된다.
  // 세번째 단계의 출력 중 두번째를 네번째 단계에서 처리할 때,
  // 네번째 1 주위에 있는 지뢰의 수와 실제로 계산해왔던 답에서 네번째 1 주위에 있는 깃발의 수가 같으므로
  // 조합에서 사용할 깃발은 없고, 빈칸이 없으므로 조합에서 사용할 빈칸도 없다.
  // 답은 다 만들어졌는데 구현상세로서, 이미 갖추어진 조합에서 조합후보없이 조합을 만들게 되고, 따라서 입력조합과 같은 조합이 출력된다.
  def solve[P](
    nextPositionF: P => Stream[P],
    inF: P => Boolean
  )(
    size: P,
    gameMap: Map[P, GameCell]
  )(result: Stream[(Stream[P], Map[P, Boolean])])(remaining: Stream[P]): Stream[(Stream[P], Map[P, Boolean])] = {
    remaining match {
      case p #:: ps =>
        val newResult =
          result.flatMap {
            case (originP, pMap) =>
              // Console.err.println("===")
              val allUnknownCells =
                nextPositionF(p)
                  .filter(inF)
                  .filter { p =>
                    gameMap.get(p) match {
                      case Some(UnknownCell(_)) => true
                      case _ => false
                    }
                  }
              // Console.err.println(s"remaining ${remaining} p ${p}")
              val combinatorTemplate = allUnknownCells.map(pMap.get(_))
              // 주위에 꽂아 놓은 깃발 개수는 조합틀만 보고 판단할 것이 아니라
              // 게임판도 같이 보고 판단해야 한다.
              val markedAsMinCellsInCombinatorTemplate =
                allUnknownCells
                  .map(p => pMap.get(p).map(x => (p, x)))
                  .filter {
                    case Some((_, true)) => true
                    case _ => false
                  }
                  .map { case Some((p, _)) => p }
              val markedAsMineCellsInMap =
                nextPositionF(p)
                  .filter(inF)
                  .filter { p =>
                    gameMap.get(p) match {
                      case Some(ExpectedAsMineCell(_)) => true
                      case _ => false
                    }
                  }
              val countExpectedAsMineCell =
                markedAsMinCellsInCombinatorTemplate.toSet
                  .union(markedAsMineCellsInMap.toSet)
                  .size

              val alreadyConsideredCells =
                allUnknownCells.filter { p =>
                  pMap.get(p) match {
                    case Some(_) => true
                    case _ => false
                  }
                }
              val notYetConsideredUnknownCells = allUnknownCells.diff(alreadyConsideredCells)
              val notYetConsideredUnknownCellsCount = notYetConsideredUnknownCells.size
              // Console.err.println(s"aCC ${alreadyConsideredCells} nYCUCC ${notYetConsideredUnknownCellsCount} cEAMC ${countExpectedAsMineCell}")
              val aroundMineCount =
                gameMap.get(p) match {
                  case Some(CleanCell(aroundMineCount)) => aroundMineCount
                }
              // 이 값이 0 보다 작아질 수도 있다. 무슨 말인가.
              // 현재 계산 위치를 기준으로 지금까지 주위에 꽂았던 깃발 개수가 현재 위치 주위의 지뢰 개수보다 많을 수 있다.
              // 이와 같은 경우 계산 실패이다.
              val remainingExpectedMineCellsCount = aroundMineCount - countExpectedAsMineCell
              val remainingExpectedCleanCellsCount = notYetConsideredUnknownCellsCount - remainingExpectedMineCellsCount

              val combinatorCandidates =
                Stream(
                  Stream.iterate(true, remainingExpectedMineCellsCount)(identity),
                  Stream.iterate(false, remainingExpectedCleanCellsCount)(identity)
                ).flatten
              // Console.err.println(s"aUC ${allUnknownCells.length} aMC ${aroundMineCount} rEMCC ${remainingExpectedMineCellsCount} rECCC ${remainingExpectedCleanCellsCount}")
              // Console.err.println(combinatorTemplate)
              // Console.err.println(combinatorCandidates)
              // 주위에 깃발을 꽂을 빈칸이 없는데 지뢰는 있다고 예상되는 경우는 계산실패로 다룬다.
              // 예를 들어
              // 1 1 1
              // ? ? ? 에서 두번째 계산까지 끝낸 후 t f f 조합이 만들어졌는데
              // 세번째 계산에서 주위에 지뢰가 있다고 판단되지만 깃발을 꽃을 자리는 없다.
              // 또한, 주위에 있는 지뢰수가 주위에 꽂았던 깃발개수보다 같거나 많아야 한다.
              val resultCombinator =
                if ((notYetConsideredUnknownCellsCount >= remainingExpectedMineCellsCount) &&
                  (remainingExpectedMineCellsCount >= 0))
                  Combinator.orderedUniqueCombinator(combinatorTemplate)(combinatorCandidates)
                    .toSet
                else Set()
              val result =
                resultCombinator
                  .map(xs => allUnknownCells.zip(xs).filter(x => notYetConsideredUnknownCells.contains(x._1)))
                  .map(xs => pMap ++ xs.toMap)
                  .filterNot(_.isEmpty)
                  .map(m => (p #:: originP, m))
              // Console.err.println(s"result ${result}")
              result
          }
        solve(nextPositionF, inF)(size, gameMap)(newResult)(ps)
      case Stream.Empty =>
        result
    }
  }

  def determineCompleted[P](nextPositionF: P => Stream[P], inF: P => Boolean)(map: Map[P, GameCell])(p: P): Boolean = {
    map.get(p) match {
      case Some(CleanCell(aroundMineCellCount)) =>
        nextPositionF(p).filter(inF).map(map.get).count {
          case Some(ExpectedAsMineCell(_)) => true
          case _ => false
        } == aroundMineCellCount
      case _ => false
    }
  }

  def tryToSolveThenPushToBeSteppedOn[P](nextPositionF: P => Stream[P], inF: P => Boolean, executionContext: ExecutionContext)(fsmOutput: FSMOutput[P])(gameState: GameState[P]): (GameState[P], Either[Unit, Option[FSMOutput[P]]]) = {
    fsmOutput match {
      case AdjacentPositionPowerSet(psss) =>
        val answerFuture =
          psss
            .map { pss =>
              // Future 를 이 단계에서 써야, 각 좌표열마다 한 개씩 병렬처리를 시도하게 된다.
              // 더 안의 solve 바로 앞에서 Future 를 쓰면 각 좌표열의 모든 좌표조합마다 한 개씩 병렬처리를 시도하게 된다.
              // 취지는, 분리되는 좌표열마다 따로 계산을 수행하도록 하는 것이다.
              // ? 1 0 0 0
              // 1 1 0 0 0
              // 0 0 0 0 0
              // 0 0 0 1 1
              // 0 0 0 1 ? 과 같으면, 왼쪽 위 1 1 1 과 오른쪽 아래 1 1 1 이 서로 다른 좌표열이 된다.
              Future {
                // allPss 는 왜 구하는가. 실패하는 경우, 가능했던 조합을 모두 보이기 위해 쓸 자료이다.
                val allPss =
                  pss
                    .map { ps =>
                      solve(nextPositionF, inF)(gameState.gameMap.size, gameState.gameMap.map)(Stream((Stream.Empty, Map[P, Boolean]())))(ps)
                    }
                val answerPss =
                  allPss
                    .dropWhile(_.filterNot(_._2.isEmpty).length != 1)
                    .headOption
                (answerPss, allPss)
              }(executionContext)
            }
            .foldLeft(Future.successful(Stream[(Stream[(Stream[P], Map[P, Boolean])], Stream[Stream[(Stream[P], Map[P, Boolean])]])]())) {
              case (result, f) =>
                result.flatMap { result =>
                  f.map {
                    case (Some(result2), allPss) => (result2, allPss) #:: result
                    case (_, allPss) => (Stream.Empty, allPss) #:: result
                  }(executionContext)
                }(executionContext)
            }
            .map(_.reverse)(executionContext)
        val answer = Await.result(answerFuture, Duration.Inf)
        val answerFirst = answer.map(_._1).filter(_.length == 1)
        val resultGameState =
          answerFirst
            .foldLeft(gameState) {
              case (gameState, list) =>
                val (newBeSteppedOnCell, newBeSteppedOnCellLookup, newMap) =
                  list.foldLeft(
                    (gameState.beSteppedOnCell, gameState.beSteppedOnCellLookup, gameState.gameMap.map)
                  ) {
                      case (result, (_, map)) =>
                        map.foldLeft(result) {
                          case ((q, s, m), (p, false)) if !s.contains(p) => (q.enqueue(p), s + p, m)
                          // 이미 밟을 예정 칸 목록에 있으면 아무것도 하지 않는다.
                          case ((q, s, m), (p, false)) => (q, s, m)
                          case ((q, s, m), (p, true)) =>
                            // 지뢰가 있다고 예상되는 장소를 찾았으면 깃발을 꽂는다.
                            val nM =
                              m.get(p) match {
                                case Some(cell) => m + (p -> ExpectedAsMineCell(cell))
                              }
                            // 깃발을 꽂은 칸을 중심으로 완료된 칸을 찾아서, 밟을 예정 칸에 넣는다.
                            val (nQ, nS) = findUnknownCellAroundCompleted(nextPositionF, inF)(p, q, s, nM)
                            (nQ, nS, nM)
                        }
                    }
                gameState.copy(
                  gameMap = gameState.gameMap.copy(map = newMap),
                  beSteppedOnCell = newBeSteppedOnCell,
                  beSteppedOnCellLookup = newBeSteppedOnCellLookup
                )
            }
        if (answerFirst.isEmpty)
          (resultGameState, Right(Some(NotFoundAnswer(answer.map(_._2)))))
        else
          (resultGameState, Right(Some(FoundAnswer(answerFirst.flatten))))
      case _ => Tuple2(gameState, Left(()))
    }
  }
}
