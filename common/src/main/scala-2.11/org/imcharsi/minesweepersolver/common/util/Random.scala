/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.util

import org.imcharsi.minesweepersolver.common.data.GameMap.{ MineCell, GameCell }
import org.imcharsi.minesweepersolver.common.data.Position2d.Position

import scala.collection.mutable

/**
 * Created by Kang Woo, Lee on 9/27/16.
 */
object Random {
  // https://en.wikipedia.org/wiki/Linear_congruential_generator
  def random(seed: Int): Int = {
    val a = 8121
    val m = 134456
    val c = 28411
    val r = (a * seed + c) % m
    r
  }

  def putMineCell(randomF: Int => Int)(size: Position, count: Int, seed: Int, forbidden: Set[Position]): (Map[Position, GameCell], Int) = {
    val positionList =
      0.until(size.x).foldLeft(List[Position]()) { (positionList, x) =>
        0.until(size.y).foldLeft(positionList) { (positionList, y) =>
          Position(x, y) :: positionList
        }
      }
    val positionListLength = size.x * size.y
    def infinitePositionStream(): Stream[Position] = positionList.toStream.append(infinitePositionStream())
    // 좌표를 만드는 방법은, 무한 좌표열을 만든 뒤, 난수만큼 좌표열에서 좌표를 건너뛴 후의 자표를 구하는 식이다.
    // 중복이 있으면 중복이 없을 때까지 계속 반복한다.
    def drop(stream: Stream[Position], collectedPosition: Set[Position], dropCount: Int): Stream[Position] = {
      if (collectedPosition(stream.head) || forbidden(stream.head))
        drop(stream.tail, collectedPosition, dropCount)
      else if (dropCount <= 0)
        stream
      else
        drop(stream.tail, collectedPosition, dropCount - 1)
    }
    val (newResult, _, newSeed) =
      0.until(count).foldLeft((Set[Position](), infinitePositionStream(), seed)) {
        case ((result, stream, seed), _) =>
          val newSeed = randomF(seed)
          val newRandom = if (newSeed < 0) newSeed * -1 else newSeed
          val newStream = drop(stream, result, newRandom % positionListLength)
          val newResult = result + newStream.head
          (newResult, newStream, newSeed)
      }
    (newResult.map(x => (x, MineCell)).toMap, newSeed)
  }
}
