/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.data

/**
 * Created by Kang Woo, Lee on 9/21/16.
 */
object Position2d {

  sealed case class Position(x: Int, y: Int) {
    // 이와 같이 OOP 문법으로 구성하면 편하다.
    def xTo(x: Int) = copy(x = x)

    def yTo(y: Int) = copy(y = y)

    def addX(n: Int) = copy(x = x + n)

    def addY(n: Int) = copy(y = y + n)

    def +(a: Position) = copy(x + a.x, y + a.y)
  }

  val origin = Position(0, 0)

  val allAroundPositionF: Definition.AllAroundPositionF[Position] =
    (p: Position) => {
      val variation = Stream(-1, 0, 1)
      variation
        .flatMap(x => variation.map(y => (x, y)))
        .filter {
          case (0, 0) => false
          case _ => true
        }
        .map { case (x, y) => Position(p.x + x, p.y + y) }
    }

  val allAdjacentPositionF: Definition.AllAroundPositionF[Position] =
    (p: Position) => Stream(p.addX(1), p.addY(1), p.addX(-1), p.addY(-1))

  // 여기서 '인접'의 의미는, 수직/수평 방향으로 닿았을 때 이다. 대각선으로 닿은 것은 '인접'이 아니다.
  def assertAdjacent(prev: Position, next: Position): Boolean = {
    val x = prev.x
    val y = prev.y
    val targetX = next.x
    val targetY = next.y
    (x - 1 == targetX && y == targetY) ||
      (x + 1 == targetX && y == targetY) ||
      (x == targetX && y - 1 == targetY) ||
      (x == targetX && y + 1 == targetY)
  }

  def inF(size: Position)(p: Position): Boolean = (p.x >= 0 && p.y >= 0 && p.x < size.x && p.y < size.y)
}
