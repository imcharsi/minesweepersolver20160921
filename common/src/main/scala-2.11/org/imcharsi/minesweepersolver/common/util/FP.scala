/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.util

/**
 * Created by Kang Woo, Lee on 9/23/16.
 */
object FP {

  trait Functor[F[_]] {
    def fmap[A, B](f: A => B): F[A] => F[B]
  }

  trait Applicative[F[_]] {
    def appl[A, B](f: F[A => B]): F[A] => F[B]
  }

  trait Monad[F[_]] {
    def unit[A](a: A): F[A]

    def bind[A, B](f: A => F[B]): F[A] => F[B]
  }

  trait CommonMonad[F[_]] extends Monad[F] with Functor[F] with Applicative[F] {
    override def fmap[A, B](f: A => B): F[A] => F[B] = bind(f.andThen(unit))

    override def appl[A, B](f: F[A => B]): F[A] => F[B] = fa => bind((f: A => B) => fmap(f)(fa))(f)
  }

  trait StateCommonMonad[F[_, _], S]
    extends CommonMonad[({ type X[A] = F[S, A] })#X]
    with Functor[({ type X[A] = F[S, A] })#X]
    with Applicative[({ type X[A] = F[S, A] })#X]

  sealed case class State[S, A](f: S => (S, A))

  class StateMonad[S] extends StateCommonMonad[State, S] {
    type X[S, A] = State[S, A]

    override def unit[A](a: A): X[S, A] = State((s: S) => (s, a))

    override def bind[A, B](f: (A) => X[S, B]): (X[S, A]) => X[S, B] =
      sa => State {
        s =>
          {
            val (s1, a) = sa.f(s)
            val (s2, b) = f(a).f(s1)
            (s2, b)
          }
      }
  }

  class StateEitherOptionMonad[S, L] extends StateCommonMonad[({ type X[S, R] = State[S, Either[L, Option[R]]] })#X, S] {
    type X[S, R] = State[S, Either[L, Option[R]]]

    override def unit[R](r: R): X[S, R] = State((s: S) => (s, Right(Some(r))))

    override def bind[A, B](f: (A) => X[S, B]): (X[S, A]) => X[S, B] =
      sa => State[S, Either[L, Option[B]]] {
        s =>
          {
            val (s1, a) = sa.f(s)
            a match {
              case Right(Some(a)) =>
                val (s2, b) = f(a).f(s1)
                (s2, b)
              case Right(None) =>
                (s1, Right(None))
              case Left(l) =>
                (s1, Left(l))
            }
          }
      }

    def orElse[A](sF: (S, S, Either[L, Option[A]]) => S)(first: X[S, A])(orElse: => X[S, A]): X[S, A] =
      State { s =>
        val (s1, a) = first.f(s)
        a match {
          case Right(Some(_)) => (s1, a)
          case _ => orElse.f(sF(s, s1, a))
        }
      }

    def repeat[A](continueF: (S, Either[L, Option[A]]) => Boolean, procF: X[S, A] => X[S, A])(init: X[S, A]): X[S, A] =
      State { s =>
        def innerF(lastS: S, lastA: Either[L, Option[A]]): (S, Either[L, Option[A]]) = {
          if (continueF(lastS, lastA)) {
            val (newS, newA) = procF(State(s => (s, lastA))).f(lastS)
            innerF(newS, newA)
          } else (lastS, lastA)
        }
        val (initS, initA) = init.f(s)
        innerF(initS, initA)
      }
  }

}
