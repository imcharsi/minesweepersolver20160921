/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.combinator

/**
 * Created by Kang Woo, Lee on 9/21/16.
 */
object Combinator {
  // 이 기능은, 1,2,3,4 가 주어졌을 때,
  // 1,(2,3,4)
  // 2,(3,4)
  // 3,(4)
  // 4,() 와 같이 자른다.
  // 이 말은, 앞의 원소와 뒤의 조합후보로 자른다는 말이고, 1,(2,3,4) 의 경우
  // 1,2
  // 1,3
  // 1,4 와 같은 조합을 만들 수 있게 된다는 말이다.
  def splitF[X](result: Stream[(X, Stream[X])])(xs: Stream[X]): Stream[(X, Stream[X])] =
    xs match {
      case x #:: tail => splitF((x, tail) #:: result)(tail)
      case Stream.Empty => result
    }

  // 햇갈릴 수 있는데, 동작방식에 관해 하나씩 살펴보자.
  // 우선, 1,2,3,4 로 순서없고 중복없는 조합을 만드는 것을 예로 들면, 먼저 구하려는 조합은
  // 1 2 3
  // 1 2 4
  // 1 3 4
  // 2 3 4 가 되어야 한다.
  // 그런데, 1,2,3,4 로 splitF 를 하면 아래와 같이 된다.
  // 1,(2,3,4)
  // 2,(3,4)
  // 3,(4)
  // 4,()
  // 여기서 3 과 4 는 뒤에 이어지는 조합 후보들을 사용해서 2 개의 원소를 추가로 만들 수 없으므로 결국 제외된다.
  // 첫 번째 splitF 결과인 1,(2,3,4) 를 예로 들어서, 다시 2,3,4 로 splitF 를 하면
  // 2,(3,4)
  // 3,(4)
  // 4,() 가 되고, 이 결과를 앞의 1 과 붙이면
  // (1,2),(3,4)
  // (1,3),(4)
  // (1,4),() 와 같이 된다. 1,4 는 이어지는 조합 후보가 없으므로 제외된다.
  // 다시 (1,2),(3,4) 의 3,4 로 splitF 를 하면
  // 3,(4)
  // 4,() 가 되고, 이를 앞의 (1,2) 와 붙이면
  // (1,2,3),(4)
  // (1,2,4),() 와 같이 된다. 개수를 전부 채웠으므로 뒤의 (4) 는 버린다.
  // 이와 같은 방식을 반복하여 개수를 채울 때까지 반복하면, 순서없고 중복없는 조합이 만들어지게 된다.
  // 순서있고 중복없는 조합은 어떻게 만드는가. 0,1,2,3 을 예로 들면 아래와 같다.
  // 0, 1, 2
  // 0, 1, 3
  // 0, 2, 1
  // 0, 2, 3
  // 0, 3, 1
  // 0, 3, 2
  // 1, 0, 2
  // 1, 0, 3
  // 1, 2, 0
  // 1, 2, 3
  // 1, 3, 0
  // 1, 3, 2
  // 2, 0, 1
  // 2, 0, 3
  // 2, 1, 0
  // 2, 1, 3
  // 2, 3, 0
  // 2, 3, 1
  // 3, 0, 1
  // 3, 0, 2
  // 3, 1, 0
  // 3, 1, 2
  // 3, 2, 0
  // 3, 2, 1
  // 원리는 똑같은데, splitF 에서 사용되는 인자만 달라진다.
  // 순서없고 중복없는 조합에서는 위의 조합에서 사용된 원소가 아래의 조합후보에서 빠지는데,
  // 순서있고 중복없는 조합에서는 위의 조합에서 사용된 원소가 아래의 조합후보에서 빠지지 않는다.
  // 그러나, 이미 사용된 원소는 같은 조합 안에서는 빠진다. 무슨 말인가.
  // 0,(1,2,3)
  // 1,(0,2,3)
  // 2,(0,1,3)
  // 3,(0,2,3) 와 같이, 0,(1,2,3) 줄을 보면 0 은 뒤의 조합후보에서는 사용되지 않는데 다음 줄의 조합후보에서는 사용된다.
  // 순서있고 중복있는 조합도 원리는 똑같은데 역시 인자가 달라진다.
  // 0,(0,1,2,3)
  // 1,(0,1,2,3)
  // 2,(0,1,2,3)
  // 3,(0,1,2,3) 과 같이 조합후보가 항상 전체 조합후보가 되도록 맞춘다.
  // 첫 줄의 0,(0,1,2,3) 에서 0,1,2,3 을 다시 splitF 하고 조합후보가 전체 조합후보가 되도록 맞추면
  // 0,(0,1,2,3)
  // 1,(0,1,2,3)
  // 2,(0,1,2,3)
  // 3,(0,1,2,3) 가 되고, 앞의 0 과 붙이면
  // (0,0),(0,1,2,3)
  // (0,1),(0,1,2,3)
  // (0,2),(0,1,2,3)
  // (0,3),(0,1,2,3) 가 된다. 또 다시 첫번째 줄만 반복하면
  // (0,0,0),(0,1,2,3)
  // (0,0,1),(0,1,2,3)
  // (0,0,2),(0,1,2,3)
  // (0,0,3),(0,1,2,3) 가 된다. 이와 같이 각 조합의 개수가 채워질 때까지 반복하는 것이다.
  // 미리 원소를 써넣는 기능에 관해 살펴보자.
  // 조합의 두번째 원소가 항상 1 이 되도록 하고 나머지 2 칸을 2,3,4 로 순서없고 중복없는 조합을 만드는 것을 예로 들면
  // 2 1 3
  // 2 1 4
  // 3 1 4 가 되어야 한다.
  // 그런데 이 결과는 2,3,4 로 원소 개수가 2 개인 순서없고 중복없는 조합을 만든 뒤 중간에 1 을 넣는 것과 같다.
  // 이 연습에서 사용한 방법은, 원소를 써넣을 칸에 대응하는 틀을 List[Option[X]] 로 미리 만들어 놓고
  // 틀 안의 요소를 하나씩 살펴보면서 None 이면 조합의 원소로 채우고 Some 이면 주어진 값으로 채우는 것이다.
  // 이번에 쓸 칸이 Some 이면, 새로 채워넣기 직전의 조합후보를 그대로 써야, 다음 칸이 None 일 때 조합 계산을 할 수 있게 된다.
  // Code 보다 설명이 더 어렵다.

  // 이 조합은, 사실상 집합의 부분집합 중 원소의 개수가 특정한 개수인 부분집합을 구할 때도 쓸 수 있다.
  // unorderedUniqueCombinator/orderedUniqueCombinator/orderedDuplicatedCombinator 모두
  // 조합의 일부 칸에 미리 원소를 지정할 수 있도록 했다.
  // unorderedUniqueCombinator/orderedUniqueCombinator 에서는 template 의 Some 이 해당 자리에 미리 써넣을 원소이고
  // orderedDuplicatedCombinator 에서는 xs 의 Right 가 해당 자리에 미리 써넣을 원소이다.
  // 이와 같이 하면, 필요없는 조합은 처음부터 계산하지 않아도 되는 장점이 있다.
  def unorderedUniqueCombinator[X](template: Stream[Option[X]])(xs: Stream[X]): Stream[Stream[X]] = {
    // impo 보통의 recursive 보다 tail-recursive 방법이 더 단순하다. co-recursive 는 가능한가.
    // co-recursive 로 하는 것은 가능은 할지 몰라도, 이익은 없을 듯 하다.
    // 왜냐하면, 각 조합이 일정 개수의 원소를 갖게 될 때까지는 출력을 할 수 없기 때문이다.
    // 부분 집합처럼, 매 실행마다 결론이 나오는 것이 아니다.
    def innerF(result: Stream[(Stream[X], Stream[X])])(template: Stream[Option[X]]): Stream[Stream[X]] = {
      template match {
        case Some(templateX) #:: templateXs =>
          innerF(
            result
              .map {
                case (resultXs, candidateXs) =>
                  (templateX #:: resultXs, candidateXs)
              }
          )(templateXs)
        case None #:: templateTail =>
          innerF(
            result
              .flatMap {
                case (result, candidate) =>
                  splitF(Stream.Empty)(candidate).map { case (splitX, splitXs) => (splitX #:: result, splitXs) }
              }
          )(templateTail)
        case Stream.Empty => result.map(_._1)
      }
    }
    // 아래와 같이 뒤집어서 시작하면, 그 다음부터는 뒤집을 필요 없이 순서가 맞춰져 있게 된다.
    innerF(Stream((Stream.Empty, xs.reverse)))(template.reverse)
  }

  def dropFirst[X](beDroppedX: X, xs: Stream[X]): Stream[X] = {
    def innerF(result: Stream[X])(remaining: Stream[X]): Stream[X] = {
      remaining match {
        case x #:: xs if x == beDroppedX => xs.foldLeft(result)((r, x) => x #:: r).reverse
        case x #:: xs => innerF(x #:: result)(xs)
        case Stream.Empty => result
      }
    }
    innerF(Stream.Empty)(xs)
  }

  def orderedUniqueCombinator[X](template: Stream[Option[X]])(xs: Stream[X]): Stream[Stream[X]] = {
    def innerF(result: Stream[(Stream[X], Stream[X])])(template: Stream[Option[X]]): Stream[Stream[X]] = {
      template match {
        case Some(templateX) #:: templateXs =>
          innerF(
            result
              .map {
                case (resultXs, candidateXs) =>
                  (templateX #:: resultXs, candidateXs)
              }
          )(templateXs)
        case None #:: templateXs =>
          innerF(
            result
              .flatMap {
                case (result, candidate) =>
                  splitF(Stream.Empty)(candidate)
                    // unorderedUniqueCombinator 와 거의 같은데, 아래의 구절이 다르다.
                    // 햇갈릴 수 있다.
                    // candidate 가 1,2,3,4 이면
                    // 1,(2,3,4)
                    // 2,(1,3,4)
                    // 3,(1,2,4)
                    // 4,(1,2,3) 이 된다. 아래의 candidate.filterNot ... 구절이 이 뜻이다.
                    // 한편, 아래의 dropFirst 는 왜 필요한가.
                    // xs={true,true} 이면, 첫번째 splitX 인 true 에 대해 candidate.filterNot(_==splitX) 라 하면
                    // splitX 가 전부 지워지는 문제가 생긴다. 첫번째 splitX 만 지워져야 한다.
                    // orderedUniqueCombinator 의 'unique' 뜻은, xs 의 각 x 가 결과에서 유일해야 한다는 말이다.
                    // xs 에서 동일성을 기준으로 봤을 때 같은 x 들이 있다 해도 순서가 다른 x 는 다른 것으로 본다.
                    .map { case (splitX, splitXs) => (splitX #:: result, dropFirst(splitX, candidate)) }
              }
          )(templateXs)
        case Stream.Empty => result.map(_._1)
      }
    }
    innerF(Stream((Stream.Empty, xs.reverse)))(template.reverse)
  }

  // 해당 위치에 미리 값을 정할 때 Right 로 쓰고, 나머지 위치에는 Left 로 쓴 값들이 조합된다.
  def orderedDuplicatedCombinator[X](xs: Stream[Either[X, X]]): Stream[Stream[X]] = {
    val xsReverse = xs.reverse
    val template = xsReverse.map(_.right.toOption)
    val xs1: Stream[X] = xsReverse.flatMap(_.left.toOption.toList)
    def innerF(result: Stream[(Stream[X], Stream[X])])(template: Stream[Option[X]]): Stream[Stream[X]] = {
      template match {
        case Some(templateX) #:: templateXs =>
          innerF(
            result
              .map { case (resultXs, _) => (templateX #:: resultXs, xs1) }
          )(templateXs)
        case None #:: templateXs =>
          innerF(
            result
              .flatMap {
                case (result, _) =>
                  // unorderedUniqueCombinator 와 거의 같은데, 아래의 두 구절이 다르다.
                  splitF(Stream.Empty)(xs1)
                    .map { case (splitX, splitXs) => (splitX #:: result, xs1) }
              }
          )(templateXs)
        case Stream.Empty => result.map(_._1)
      }
    }
    innerF(Stream((Stream.Empty, xs1)))(template)
  }

  // 모든 기능의 결과 예시는 검사에 썼다.
  def append[X](xs: Stream[X], x: X): (Stream[X], Stream[X]) = (xs, x #:: xs)

  // 이 기능은 부분집합을 계산한다.
  // 이 기능은 이 연습에서만 쓸 것이므로, 특수한 형태의 부분집합을 만드는 데 쓸 수 있도록 형을 바꿨다.
  // 부분집합을 이루는 각 원소들이 특정 기준에 따라 서로 인접한 것만 남도록 한다.
  def adjacentPowerSet[X](predF: (X, X) => Boolean)(xs: Stream[X]): Stream[Stream[X]] = {
    // reduce 로는, (X,X)=>Boolean 과 같은 계산을 할 수 없고,
    // foldLeft 로는 계산을 할 수는 있는데, 중간에 멈추는 것이 안 된다.
    def reduceF(result: Boolean, prevX: Option[X])(xs: Stream[X]): Boolean = {
      xs match {
        case x2 #:: xsTail =>
          prevX match {
            case Some(x1) if predF(x1, x2) && result => reduceF(true, Some(x2))(xsTail)
            case None => reduceF(true, Some(x2))(xsTail)
            case _ => false
          }
        case _ =>
          result
      }
    }
    def appendF(xs: Stream[X], x: X): Stream[Stream[X]] = xs #:: Stream(x #:: xs)
    def innerFx(result: Stream[Stream[X]], xs: Stream[X]): Stream[Stream[X]] = {
      xs match {
        case x #:: xs => innerFx(result.flatMap(xs => appendF(xs, x)), xs)
        case Stream.Empty =>
          result
            .flatMap { xs =>
              orderedUniqueCombinator(xs.map(_ => None))(xs)
                .filter(reduceF(true, None)).headOption match {
                  case Some(xs) => Stream(xs)
                  case None => Stream.Empty
                }
            }
      }
    }
    // 구할 부분집합이 많아질 것으로 예상된다. 따라서, co-recursive 로 할 수 있는 것은 그렇게 하는 것이 좋겠다.
    // 위의 조합과 다르게, 부분집합에서는 각 단계마다 출력할 것이 있고,
    // 현재 단계의 출력이 다음 단계의 입력이 될 수 있도록 구성하는 것도 가능하다.
    /*    def innerF(prevTotal: List[List[X]], xs: List[X]): Stream[List[X]] = {
      xs match {
        case x :: xsTail =>
          def innerF2(resultTotal: List[List[X]], resultCurrent: List[List[X]])(remainTotal: List[List[X]]): (List[List[X]], List[List[X]]) = {
            remainTotal match {
              case remainX :: remainTail =>
                val n = x :: remainX
                orderedUniqueCombinator(n.map(_ => None))(n)
                  .filter(reduceF(true, None)(_))
                  .headOption match {
                    case Some(n) =>
                      innerF2(n :: resultTotal, n :: resultCurrent)(remainTail)
                    case _ =>
                      innerF2(resultTotal, resultCurrent)(remainTail)
                  }
              case Nil =>
                (resultTotal, resultCurrent)
            }
          }
          val (newTotal, newResult) = innerF2(prevTotal, List())(prevTotal)
          newResult.toStream.append(innerF(newTotal, xsTail))
        case Nil =>
          Stream.empty
      }
    }*/
    // xs={1,2,3} 로 innerF 를 첫번째 실행한 결과는
    // newTotal=List(List(), List(1)) 이 되고, xsTail={2,3} 로 재귀실행한다. 또 다시 실행한 결과는
    // newTotal=List(List(), List(1), List(2), List(2,1)) 이 되고, xsTail={3} 으로 재귀실행한다. 또 다시 실행한 결과는
    // newTotal=List(List(), List(1), List(2), List(2,1),List(3), List(3,1), List(3,2), List(3,2,1)) 이 되고 xsTail={} 으로 재귀실행한다. 또 다시 실행한 결과는
    // xs={} 이므로 마친다.
    // 여기서 각 단계별 출력은, 추가로 늘어나는 것이다. 첫번째 실행한 결과는
    // List(List()) 에서 List(List(), List(1)) 로 달라졌으므로 이 단계의 출력은 List(List(1)) 이다.
    // 두번째 실행한결과는
    // List(List(), List(1)) 에서 List(List(), List(1), List(2), List(2,1)) 로 달라졌으므로 이 단계의 출력은 List(List(2), List(2,1)) 이다.
    // 세번째 실행한 결과는
    // List(List(), List(1), List(2), List(2,1)) 에서 List(List(), List(1), List(2), List(2,1),List(3), List(3,1), List(3,2), List(3,2,1)) 로 달라졌으므로 이 단계의 출력은 List(List(3), List(3,1), List(3,2), List(3,2,1)) 이다.
    // innerF 의 역할은 xs 를 회전하는 것이고,
    // innerF2 의 역할은 여태까지의 부분집합 계산결과인 prevTotal 에 이번 x 를 붙인 뒤, 이번 단계에서만 늘어난 것을 분리하는 것이다.
    // 이렇게 분리한 것이 현재 계산단계의 출력이 된다.
    // 지금까지의 설명이 부분집합을 구하는 방식에 대한 설명이고, 아래부터는 이번 연습에서만 쓸 인접한 부분집합을 구하는 방식에 대한 설명이다.
    // 위의 방식에 따라 계산한 각 부분집합 안의 모든 원소들을 조합후보로 하여 순서있고 중복없는 조합을 구한 뒤,
    // 각 조합의 원소들이 서로 인접한 경우만 남긴다.
    // 이는 이번 연습에서, 주어진 좌표들로 만든 부분집합 각각의 원소들이, 서로 인접하게 되는 순서로 배치되도록 하기 위해 필요하다.
    // 더 좋은 방법없나. 빠른 방법은 아닌 듯 하다.
    // 계산을 어렵게 하는 예는, 아래와 같은 위치로 좌표가 주어졌을 때, 끝을 어떻게 정하는가이다.
    // aabbcc
    // ddee..
    // adebc, cbeda, cbade, edabc 등이 서로 인접한 순서가 되는데,
    // 이것을 지금과 같은 방법이 아닌 다른 방법으로 구하는 방법을 모르겠다.
    // 만약, 좌표가 2차원 평면이 아니라 3차원 공간이 되면 더 복잡해진다.
    // 한편, 지금의 연습에서는, 인접한 순서를 하나만 구하면 된다.
    // 계산으로는 정방향 순서와 반대방향 순서로 두개가 만들어지는데, 둘 다 동일한 열이기 때문이다.

    //    List() #:: innerF(List(List()), xs)
    innerFx(Stream(Stream.Empty), xs)
  }
}
