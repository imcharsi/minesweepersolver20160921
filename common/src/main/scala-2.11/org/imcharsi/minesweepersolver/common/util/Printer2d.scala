/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.util

import org.imcharsi.minesweepersolver.common.data.GameMap._
import org.imcharsi.minesweepersolver.common.data.Position2d.Position

/**
 * Created by Kang Woo, Lee on 9/24/16.
 */
object Printer2d {
  def mapToString(size: Position, map: Map[Position, GameCell]): String = {
    val stringBuilder = StringBuilder.newBuilder
    0.until(size.y).foldLeft(stringBuilder) {
      case (stringBuilder, y) =>
        0.until(size.x).foldLeft(stringBuilder) {
          case (stringBuilder, x) =>
            map.get(Position(x, y)) match {
              case Some(UnknownCell(_)) => stringBuilder.append(" ?")
              case Some(CleanCell(count)) => stringBuilder.append("%2d".format(count))
              case Some(MineCell) => stringBuilder.append(" M")
              case Some(ExpectedAsMineCell(_)) => stringBuilder.append(" E")
              case None => stringBuilder.append("..")
            }
        }.append('\n')
    }.toString()
  }

}
