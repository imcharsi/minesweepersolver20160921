/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.util

import org.imcharsi.minesweepersolver.common.data.Position2d
import org.imcharsi.minesweepersolver.common.data.Position2d.Position
import org.scalatest.{ Matchers, FunSuite }

/**
 * Created by Kang Woo, Lee on 9/27/16.
 */
class TestRandom extends FunSuite with Matchers {
  test("putMineCell") {
    // 각 칸마다 한개씩 넣어서 게임판을 임의로 16 개를 만들때, 지금 구현에서는 13 개의 고유한 게임판이 만들어진다.
    1.to(16)
      .map(Random.putMineCell(Random.random)(Position(4, 4), 1, _, Set()))
      .map(_._1)
      .toSet
      .size should equal(13)
    // 특정 칸은 빼고 게임판을 만들 때, 반드시 특정 칸이 빠지는지 확인하려면,
    // 특정칸을 제외한 나머지 칸을 모두 지뢰로 채웠을 때 게임판이 한개만 만들어지는지 확인하면 된다.
    1.to(10)
      .map(Random.putMineCell(Random.random)(Position(2, 2), 3, _, Set(Position(0, 0))))
      .map(_._1)
      .toSet
      .size should equal(1)
  }
}
