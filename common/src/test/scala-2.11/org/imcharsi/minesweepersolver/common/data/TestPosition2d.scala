/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.data

import org.scalatest.{ Matchers, FunSuite }

/**
 * Created by Kang Woo, Lee on 9/21/16.
 */
class TestPosition2d extends FunSuite with Matchers {
  test("allAroundPositionF") {
    val variation = Set(-1, 0, 1)
    val expected =
      variation
        .flatMap(x => variation.map(y => Position2d.Position(x, y)))
        .filter(_ != Position2d.origin)
    val result = Position2d.allAroundPositionF(Position2d.Position(0, 0)).toSet
    result should equal(expected)
  }

  test("origin") {
    Position2d.origin should equal(Position2d.Position(0, 0))
  }

  test("xTo") {
    Position2d.origin.xTo(2) should equal(Position2d.Position(2, 0))
  }

  test("yTo") {
    Position2d.origin.yTo(2) should equal(Position2d.Position(0, 2))
  }

  test("addX") {
    Position2d.origin.xTo(1).addX(1) should equal(Position2d.Position(2, 0))
  }

  test("addY") {
    Position2d.origin.yTo(1).addY(1) should equal(Position2d.Position(0, 2))
  }

  test("+") {
    (Position2d.origin + Position2d.Position(1, 1)) should equal(Position2d.Position(1, 1))
  }

  test("assertAdjacent") {
    Position2d.assertAdjacent(Position2d.Position(-1, 0), Position2d.origin) should equal(true)
    Position2d.assertAdjacent(Position2d.Position(1, 0), Position2d.origin) should equal(true)
    Position2d.assertAdjacent(Position2d.Position(0, -1), Position2d.origin) should equal(true)
    Position2d.assertAdjacent(Position2d.Position(0, 1), Position2d.origin) should equal(true)
    Position2d.assertAdjacent(Position2d.Position(1, 1), Position2d.origin) should equal(false)
    Position2d.assertAdjacent(Position2d.Position(-1, -1), Position2d.origin) should equal(false)
    Position2d.assertAdjacent(Position2d.Position(1, -1), Position2d.origin) should equal(false)
    Position2d.assertAdjacent(Position2d.Position(-1, 1), Position2d.origin) should equal(false)
    Position2d.assertAdjacent(Position2d.origin, Position2d.origin) should equal(false)
  }

  test("inF") {
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(-1, -1)) should equal(false)
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(0, -1)) should equal(false)
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(-1, 0)) should equal(false)
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(0, 0)) should equal(true)
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(0, 1)) should equal(true)
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(1, 1)) should equal(true)
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(1, 0)) should equal(true)
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(2, 1)) should equal(false)
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(1, 2)) should equal(false)
    Position2d.inF(Position2d.Position(2, 2))(Position2d.Position(2, 2)) should equal(false)
  }
}
