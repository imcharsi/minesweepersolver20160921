/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.util

import org.imcharsi.minesweepersolver.common.util.FP.{ StateEitherOptionMonad, State, StateMonad }
import org.scalatest.{ Matchers, FunSuite }

/**
 * Created by Kang Woo, Lee on 9/25/16.
 */
class TestFP extends FunSuite with Matchers {
  test("StateMonad") {
    def stateF(i: Int): State[Int, Int] = State(s => (s + i, i))
    val stateMonad = new StateMonad[Int]
    val (s, i) = stateMonad.bind(stateF)(stateMonad.unit(1)).f(1)
    s should equal(2)
    i should equal(1)
  }

  test("StateEitherOptionMonad") {
    def stateF(i: Int): State[Int, Either[Int, Option[Int]]] =
      State(s => (s + i, if (i < 5) Right(Some(i)) else if (i < 10) Right(None) else Left(i)))
    val stateEitherOptionMonad = new StateEitherOptionMonad[Int, Int]
    val (s, i) = stateEitherOptionMonad.bind(stateF)(stateEitherOptionMonad.unit(1)).f(1)
    s should equal(2)
    i should equal(Right(Some(1)))
    val (s2, i2) = stateEitherOptionMonad.bind(stateF)(stateEitherOptionMonad.unit(5)).f(1)
    s2 should equal(6)
    i2 should equal(Right(None))
    val (s3, i3) = stateEitherOptionMonad.bind(stateF)(stateEitherOptionMonad.unit(10)).f(1)
    s3 should equal(11)
    i3 should equal(Left(10))
  }

  test("StateEitherOptionMonad.orElse") {
    def stateF(i: Int): State[Int, Either[Int, Option[Int]]] =
      State(s => (s + i, if (i < 5) Right(Some(i)) else if (i < 10) Right(None) else Left(i)))
    val stateEitherOptionMonad = new StateEitherOptionMonad[Int, Int]
    def orElse = stateEitherOptionMonad.orElse[Int]((a, _, _) => a)(_)
    val (_, result1) = stateF(10).f(1)
    result1 should equal(Left(10))
    val (_, result2) = orElse(stateF(10))(stateF(1)).f(1)
    result2 should equal(Right(Some(1)))
  }

  test("StateEitherOptionMonad.repeat") {
    def stateF(i: Int): State[Int, Either[Int, Option[Int]]] =
      State(s => (s, if (i < 5) Right(Some(i)) else if (i < 10) Right(None) else Left(i)))
    def adderF(i: Int): State[Int, Either[Int, Option[Int]]] =
      State(s => (s + 1, Right(Some(i + 1))))
    val stateEitherOptionMonad = new StateEitherOptionMonad[Int, Int]
    def continueF(s: Int, a: Either[Int, Option[Int]]): Boolean =
      a match {
        case Right(Some(_)) => true
        case _ => false
      }
    val w = stateEitherOptionMonad.bind(adderF).andThen(stateEitherOptionMonad.bind(stateF))
    stateEitherOptionMonad.bind(adderF)(stateF(1))
    val (resultS, resultA) = stateEitherOptionMonad.repeat(continueF, w)(stateEitherOptionMonad.unit(1)).f(1)
    resultS should equal(5)
    resultA should equal(Right(None))
  }
}
