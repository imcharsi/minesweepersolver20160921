/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.data

import org.imcharsi.minesweepersolver.common.combinator.Combinator
import org.imcharsi.minesweepersolver.common.data.GameMap._
import org.imcharsi.minesweepersolver.common.data.Position2d.Position
import org.imcharsi.minesweepersolver.common.util.FP.{ State, StateEitherOptionMonad }
import org.imcharsi.minesweepersolver.common.util.Printer2d
import org.scalatest.{ FunSuite, Matchers }

import scala.collection.immutable.Queue
import scala.concurrent.ExecutionContext.Implicits

/**
 * Created by Kang Woo, Lee on 9/23/16.
 */
class TestGameMap extends FunSuite with Matchers {
  implicit val seom = new StateEitherOptionMonad[GameState[Position], Unit]

  test("findAllConnectedNext") {
    val expected = Set(Position(1, 1), Position(1, 2), Position(1, 3), Position(2, 1), Position(3, 1), Position(2, 3), Position(3, 3), Position(3, 2), Position(0, 1))
    val result =
      GameMap.findAllConnectedNext[Position](
        expected.contains(_),
        Position2d.allAroundPositionF
      )(Queue(Position(0, 1)))
    result should equal(expected)
  }

  test("arrangeMap") {
    val map = Map[Position, GameCell]((Position(1, 1) -> MineCell))
    val size = Position(3, 3)
    val result = GameMap.arrangeMap(Position2d.allAroundPositionF, Position2d.inF(size), Position2d.origin)(size, map)
    val expected =
      Map[Position, GameCell](
        (Position(0, 0) -> UnknownCell(CleanCell(1))),
        (Position(0, 1) -> UnknownCell(CleanCell(1))),
        (Position(0, 2) -> UnknownCell(CleanCell(1))),
        (Position(1, 0) -> UnknownCell(CleanCell(1))),
        (Position(1, 1) -> UnknownCell(MineCell)),
        (Position(1, 2) -> UnknownCell(CleanCell(1))),
        (Position(2, 0) -> UnknownCell(CleanCell(1))),
        (Position(2, 1) -> UnknownCell(CleanCell(1))),
        (Position(2, 2) -> UnknownCell(CleanCell(1)))
      )
    result should equal(expected)

    val map2 = Map[Position, GameCell]((Position(0, 0) -> MineCell), (Position(2, 2) -> MineCell))
    val result2 = GameMap.arrangeMap(Position2d.allAroundPositionF, Position2d.inF(size), Position2d.origin)(size, map2)
    val expected2 =
      Map[Position, GameCell](
        (Position(0, 0) -> UnknownCell(MineCell)),
        (Position(0, 1) -> UnknownCell(CleanCell(1))),
        (Position(0, 2) -> UnknownCell(CleanCell(0))),
        (Position(1, 0) -> UnknownCell(CleanCell(1))),
        (Position(1, 1) -> UnknownCell(CleanCell(2))),
        (Position(1, 2) -> UnknownCell(CleanCell(1))),
        (Position(2, 0) -> UnknownCell(CleanCell(0))),
        (Position(2, 1) -> UnknownCell(CleanCell(1))),
        (Position(2, 2) -> UnknownCell(MineCell))
      )
    result2 should equal(expected2)
  }

  def repeat(size: Position)(result: Queue[(GameState[Position], Either[Unit, Option[FSMOutput[Position]]])])(gameState: GameState[Position]): Queue[(GameState[Position], Either[Unit, Option[FSMOutput[Position]]])] = {
    val (newGameState, fsmOutput) = GameMap.stepOn(Position2d.allAroundPositionF, Position2d.inF(size))(gameState)
    if (newGameState.beSteppedOnCell.isEmpty)
      result.enqueue((newGameState, fsmOutput))
    else
      repeat(size)(result.enqueue((newGameState, fsmOutput)))(newGameState)
  }

  test("stepOn") {
    val size = Position(5, 5)
    val map =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map())
    val gameState =
      GameState(
        GameMap.GameMap(Position(5, 5), map),
        Queue(Position(0, 0)),
        Set(Position(0, 0)),
        Set()
      )
    val result = repeat(size)(Queue())(gameState)
    val (resultQueue, resultGameState) =
      result.foldLeft((Queue[Position](), gameState)) {
        case ((queue, _), (gameState, fsmOutput)) =>
          fsmOutput match {
            case Right(Some(SteppedOn(position))) => (queue.enqueue(position), gameState)
            case _ => (queue, gameState)
          }
      }
    val allPosition =
      (0).to(4).foldLeft(Set[Position]()) {
        case (set, y) =>
          (0).to(4).foldLeft(set) {
            case (set, x) =>
              set + Position(x, y)
          }
      }
    resultQueue.toSet should equal(allPosition)
    resultGameState.gameMap.map
      .forall { case (_, UnknownCell(_)) => false; case _ => true } should equal(true)
    resultGameState.nonZeroCleanCells.isEmpty should equal(true)
    Console.out.println(Printer2d.mapToString(resultGameState.gameMap.size, resultGameState.gameMap.map))

    val map2 =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map((Position(2, 2) -> MineCell)))
    val gameState2 =
      GameState(
        GameMap.GameMap(Position(5, 5), map2),
        Queue(Position(0, 0)),
        Set(Position(0, 0)),
        Set()
      )

    val result2 = repeat(size)(Queue())(gameState2)
    val (resultQueue2, resultGameState2) =
      result2.foldLeft((Queue[Position](), gameState2)) {
        case ((queue, _), (gameState, fsmOutput)) =>
          fsmOutput match {
            case Right(Some(SteppedOn(position))) => (queue.enqueue(position), gameState)
            case _ => (queue, gameState)
          }
      }
    resultQueue2.toSet should equal(allPosition.filterNot(_ == Position(2, 2)))
    resultGameState2.gameMap.map.filterNot(_._1 == Position(2, 2))
      .forall { case (_, UnknownCell(_)) => false; case _ => true } should equal(true)
    resultGameState2.gameMap.map(Position(2, 2)) should equal(UnknownCell(MineCell))
    resultGameState2.nonZeroCleanCells should equal(
      Set(
        Position(1, 1), Position(2, 1), Position(3, 1),
        Position(1, 2), Position(3, 2),
        Position(1, 3), Position(2, 3), Position(3, 3)
      )
    )
    Console.out.println(Printer2d.mapToString(resultGameState2.gameMap.size, resultGameState2.gameMap.map))
  }

  test("eliminateNotExposedNonZeroCleanCells") {
    val size = Position(5, 5)
    val map =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map())
    val gameState =
      GameState(
        GameMap.GameMap(
          Position(5, 5),
          map +
            (Position(0, 0) -> CleanCell(1)) +
            (Position(1, 0) -> CleanCell(1)) +
            (Position(0, 1) -> CleanCell(1)) +
            (Position(1, 1) -> CleanCell(1))
        ),
        Queue(),
        Set(),
        Set(Position(0, 0), Position(1, 0), Position(0, 1), Position(1, 1))
      )
    val (resultGameState, resultFsmOutput) =
      GameMap.eliminateNotExposedNonZeroCleanCells(Position2d.allAroundPositionF, Position2d.inF(size))(gameState)
    Console.out.println(Printer2d.mapToString(resultGameState.gameMap.size, resultGameState.gameMap.map))
    gameState.nonZeroCleanCells - Position(0, 0) should equal(resultGameState.nonZeroCleanCells)

    val map2 =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map())
    val gameState2 =
      GameState(
        GameMap.GameMap(
          Position(5, 5),
          map2 +
            (Position(1, 1) -> CleanCell(1)) +
            (Position(2, 1) -> CleanCell(1)) +
            (Position(3, 1) -> CleanCell(1)) +
            (Position(1, 2) -> CleanCell(1)) +
            (Position(2, 2) -> CleanCell(1)) +
            (Position(3, 2) -> CleanCell(1)) +
            (Position(1, 3) -> CleanCell(1)) +
            (Position(2, 3) -> CleanCell(1)) +
            (Position(3, 3) -> CleanCell(1))
        ),
        Queue(),
        Set(),
        Set(
          Position(1, 1), Position(2, 1), Position(3, 1),
          Position(1, 2), Position(2, 2), Position(3, 2),
          Position(1, 3), Position(2, 3), Position(3, 3)
        )
      )
    val (resultGameState2, resultFsmOutput2) =
      GameMap.eliminateNotExposedNonZeroCleanCells(Position2d.allAroundPositionF, Position2d.inF(size))(gameState2)
    Console.out.println(Printer2d.mapToString(resultGameState2.gameMap.size, resultGameState2.gameMap.map))
    gameState2.nonZeroCleanCells - Position(2, 2) should equal(resultGameState2.nonZeroCleanCells)
  }

  def continueF[P](gameState: GameState[P], fsmOutput: Either[Unit, Option[FSMOutput[P]]]): Boolean =
    fsmOutput match {
      case Right(Some(_)) => true
      case _ => false
    }

  def composedRepeat(size: Position) =
    seom.repeat(
      continueF,
      seom.bind((_: FSMOutput[Position]) =>
        State(GameMap.stepOn[Position](Position2d.allAroundPositionF, Position2d.inF(size))))
    )(seom.unit(NothingToDo()))

  test("computeConnectedPositionSequence") {
    val size = Position(5, 5)
    val map =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map((Position(0, 0) -> MineCell), (Position(4, 4) -> MineCell)))
    val gameState =
      GameState(
        GameMap.GameMap(Position(5, 5), map),
        Queue(Position(2, 2)),
        Set(Position(2, 2)),
        Set()
      )

    val composedF =
      seom.bind((_: FSMOutput[Position]) => State(GameMap.eliminateNotExposedNonZeroCleanCells(Position2d.allAroundPositionF, Position2d.inF(size))))
        .andThen(
          seom.bind((_: FSMOutput[Position]) => State(GameMap.computeConnectedPositionSequence(Position2d.allAdjacentPositionF)))
        )

    val (_, resultFsmOutput) =
      seom.orElse[FSMOutput[Position]]((_, b, _) => b)(composedRepeat(size))(composedF(seom.unit(NothingToDo())))
        .f(gameState)

    resultFsmOutput match {
      case Right(Some(ConnectedPositionSequence(pss))) =>
        val expected =
          Set(
            Set(Position(0, 1), Position(1, 1), Position(1, 0)),
            Set(Position(4, 3), Position(3, 3), Position(3, 4))
          )
        val result = pss.toSet
        result should equal(expected)
      case _ => fail()
    }
  }

  test("computeAdjacentPositionPowerSet") {
    val size = Position(3, 3)
    val map =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map((Position(2, 2) -> MineCell)))
    val gameState =
      GameState(
        GameMap.GameMap(size, map),
        Queue(Position(0, 0)),
        Set(Position(0, 0)),
        Set()
      )

    def composedF(size: Position) =
      seom.orElse[FSMOutput[Position]](
        (_, b, _) => b
      )(composedRepeat(size))(
          seom.bind((_: FSMOutput[Position]) => State(GameMap.eliminateNotExposedNonZeroCleanCells(Position2d.allAroundPositionF, Position2d.inF(size))))
            .andThen(
              seom.bind((_: FSMOutput[Position]) => State(GameMap.computeConnectedPositionSequence(Position2d.allAdjacentPositionF)))
            )
            .andThen(
              seom.bind(x => State(GameMap.computeAdjacentPositionPowerSet(Position2d.assertAdjacent)(x)))
            )(seom.unit(NothingToDo()))
        )

    val (resultGameState, resultFsmOutput) = composedF(size).f(gameState)

    Console.out.println(Printer2d.mapToString(resultGameState.gameMap.size, resultGameState.gameMap.map))
    resultFsmOutput match {
      case Right(Some(AdjacentPositionPowerSet(ps))) =>
        val expected =
          Set(
            Combinator.adjacentPowerSet(Position2d.assertAdjacent)(Stream(Position(1, 2), Position(1, 1), Position(2, 1)))
            .map(_.toSet).toSet
          )
        val result = ps.map(_.map(_.toSet).toSet).toSet
        result should equal(expected)
      case _ => fail()
    }

    val size2 = Position(5, 5)
    val map2 =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size2),
        Position2d.origin
      )(size, Map((Position(0, 0) -> MineCell), (Position(4, 4) -> MineCell)))
    val gameState2 =
      GameState(
        GameMap.GameMap(Position(5, 5), map2),
        Queue(Position(2, 2)),
        Set(Position(2, 2)),
        Set()
      )

    val (resultGameState2, resultFsmOutput2) = composedF(size2).f(gameState2)

    Console.out.println(Printer2d.mapToString(resultGameState2.gameMap.size, resultGameState2.gameMap.map))
    resultFsmOutput2 match {
      case Right(Some(AdjacentPositionPowerSet(ps))) =>
        val expected =
          Set(
            Combinator.adjacentPowerSet(Position2d.assertAdjacent)(Stream(Position(0, 1), Position(1, 1), Position(1, 0)))
            .map(_.toSet).toSet
          ).union(
            Set(
              Combinator.adjacentPowerSet(Position2d.assertAdjacent)(Stream(Position(3, 4), Position(3, 3), Position(4, 3)))
              .map(_.toSet).toSet
            )
          )
        val result = ps.map(_.map(_.toSet).toSet).toSet
        result should equal(expected)
      case _ => fail()
    }

    val map3 =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map())
    val gameState3 =
      GameState(
        GameMap.GameMap(size, map3),
        Queue(Position(0, 0)),
        Set(Position(0, 0)),
        Set()
      )
    val (resultGameState3, resultFsmOutput3) = composedF(size).f(gameState3)
    Console.out.println(Printer2d.mapToString(resultGameState3.gameMap.size, resultGameState3.gameMap.map))
    resultFsmOutput3 match {
      case Right(None) =>
      case _ => fail()
    }
  }

  test("solve") {
    def composedF(size: Position) =
      seom.orElse[FSMOutput[Position]](
        (_, b, _) => b
      )(composedRepeat(size))(
          seom.bind((_: FSMOutput[Position]) => State(GameMap.eliminateNotExposedNonZeroCleanCells(Position2d.allAroundPositionF, Position2d.inF(size))))
            .andThen(
              seom.bind((_: FSMOutput[Position]) => State(GameMap.computeConnectedPositionSequence(Position2d.allAdjacentPositionF)))
            )
            .andThen(
              seom.bind(x => State(GameMap.computeAdjacentPositionPowerSet(Position2d.assertAdjacent)(x)))
            )(seom.unit(NothingToDo()))
        )

    val size1X2 = Position(1, 2)
    val size2X2 = Position(2, 2)
    val size3X2 = Position(3, 2)
    val size4X2 = Position(4, 2)

    def template(size: Position, mines: Stream[Position], beSteppedOn: Stream[Position]): (GameState[Position], Stream[Stream[Map[Position, Boolean]]]) = {
      val map =
        GameMap.arrangeMap(
          Position2d.allAroundPositionF,
          Position2d.inF(size),
          Position2d.origin
        )(size, mines.map(p => (p, MineCell)).toMap)
      val gameState =
        GameState(
          GameMap.GameMap(size, map),
          Queue(beSteppedOn: _*),
          Set(beSteppedOn: _*),
          Set()
        )
      val (resultGameState, resultFsmOutput) = composedF(size).f(gameState)
      Console.out.println(Printer2d.mapToString(resultGameState.gameMap.size, resultGameState.gameMap.map))
      val psss =
        resultFsmOutput match {
          case Right(Some(AdjacentPositionPowerSet(psss))) => psss
        }
      val result =
        psss
          //          .flatMap(_.toList)
          .flatten
          .map(GameMap.solve(Position2d.allAroundPositionF, Position2d.inF(size))(resultGameState.gameMap.size, resultGameState.gameMap.map)(Stream((Stream.Empty, Map[Position, Boolean]()))))
          .map(_.map(_._2).distinct)
          .filterNot(_.filterNot(_.isEmpty).isEmpty)
          .distinct
      Console.out.println(result.mkString("\n"))
      (resultGameState, result)
    }

    // 또한, 지뢰가 있다고 판단한 칸이 실제로 지뢰가 있는 칸인지 검사해야 하지만, 상수를 썼으므로 이 검사는 생략해도 된다.
    // 검사에 나타나는 좌표 중, 답 안에서 나타나는 좌표는 전부 지뢰가 있는 칸이다.
    val (resultGameState1X2, result1X2) = template(size1X2, Stream(Position(0, 1)), Stream(Position(0, 0)))
    result1X2 should equal(Stream(Stream(Map((Position(0, 1) -> true)))))

    val (resultGameState2X2, result2X2) =
      template(
        size2X2,
        Stream(Position(1, 1)),
        Stream(Position(0, 0), Position(1, 0))
      )
    result2X2 should equal(
      List(
        List(
          Map((Position(0, 1) -> false), (Position(1, 1) -> true)),
          Map((Position(0, 1) -> true), (Position(1, 1) -> false))
        )
      )
    )

    // 1 1 1
    // ? ? ? 에서
    // 1 1 1 모두 조합하면 t f f 나 f f t 와 같은 경우를 걸러내는데,
    // 1 1 만 조합하면 t f f 나 f f t 가 나올 수 있다.
    // 그런데 이 결과는 올바른 결과이고, 주어진 좌표열에 대해 여러 개의 답이 있기 때문에 실패라고 판단해야 한다.
    val (resultGameState3X2_1, result3X2_1) =
      template(
        size3X2,
        Stream(Position(1, 1)),
        Stream(Position(0, 0), Position(1, 0), Position(2, 0))
      )
    result3X2_1.filter(_.length == 1) should equal(
      Stream(
        Stream(
          Map((Position(0, 1) -> false), (Position(1, 1) -> true), (Position(2, 1) -> false))
        )
      )
    )

    val (resultGameState3X2_2, result3X2_2) =
      template(
        size3X2,
        Stream(Position(0, 1), Position(1, 1), Position(2, 1)),
        Stream(Position(0, 0), Position(1, 0), Position(2, 0))
      )
    result3X2_2 should equal(
      Stream(
        Stream(
          Map((Position(1, 1) -> true), (Position(2, 1) -> true))
        ),
        Stream(
          Map((Position(0, 1) -> true), (Position(1, 1) -> true), (Position(2, 1) -> true))
        ),
        Stream(
          Map((Position(0, 1) -> true), (Position(1, 1) -> true))
        )
      )
    )

    val (resultGameState3X2_3, result3X2_3) =
      template(
        size3X2,
        Stream(Position(0, 1), Position(1, 1)),
        Stream(Position(0, 0), Position(1, 0), Position(2, 0))
      )
    result3X2_3.filter(_.length == 1) should equal(
      Stream(
        Stream(
          Map((Position(0, 1) -> true), (Position(1, 1) -> true))
        ),
        Stream(
          Map((Position(0, 1) -> true), (Position(1, 1) -> true), (Position(2, 1) -> false))
        )
      )
    )

    val (resultGameState3X2_4, result3X2_4) =
      template(
        size3X2,
        Stream(Position(1, 1)),
        Stream(Position(0, 0), Position(1, 0))
      )
    result3X2_4.filter(_.length == 1).length should equal(0)

    val (resultGameState4X2, result4X2) =
      template(
        size4X2,
        Stream(Position(1, 1), Position(2, 1)),
        Stream(Position(0, 0), Position(1, 0), Position(2, 0), Position(3, 0))
      )
    result4X2.filter(_.length == 1) should equal(
      Stream(
        Stream(
          Map((Position(0, 1) -> false), (Position(1, 1) -> true), (Position(2, 1) -> true), (Position(3, 1) -> false))
        )
      )
    )
  }

  test("determineCompleted") {
    val size = Position(5, 5)
    val map =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map((Position(2, 2) -> MineCell)))
    val newMap =
      map.get(Position(1, 1)) match {
        case Some(UnknownCell(inner @ CleanCell(_))) => map + (Position(1, 1) -> inner)
      }
    Console.out.println(Printer2d.mapToString(size, newMap))
    GameMap.determineCompleted(Position2d.allAroundPositionF, Position2d.inF(size))(newMap)(Position(1, 1)) should equal(false)
    val newMap2 =
      newMap.get(Position(2, 2)) match {
        case Some(UnknownCell(inner)) => newMap + (Position(2, 2) -> ExpectedAsMineCell(inner))
      }
    Console.out.println(Printer2d.mapToString(size, newMap2))
    GameMap.determineCompleted(Position2d.allAroundPositionF, Position2d.inF(size))(newMap2)(Position(1, 1)) should equal(true)
    val newMap3 =
      newMap2.get(Position(1, 2)) match {
        case Some(UnknownCell(inner)) => newMap2 + (Position(1, 2) -> ExpectedAsMineCell(inner))
      }
    Console.out.println(Printer2d.mapToString(size, newMap3))
    GameMap.determineCompleted(Position2d.allAroundPositionF, Position2d.inF(size))(newMap3)(Position(1, 1)) should equal(false)
  }

  test("tryToSolveThenPushToBeSteppedOn") {
    val size = Position(5, 5)
    val map =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map((Position(0, 4) -> MineCell), (Position(4, 4) -> MineCell)))
    val gameState =
      GameState(
        GameMap.GameMap(size, map),
        Queue(Position(0, 0)),
        Set(Position(0, 0)),
        Set()
      )

    def composedF(size: Position) =
      seom.orElse[FSMOutput[Position]](
        (_, b, _) => b
      )(composedRepeat(size))(
          seom.bind((_: FSMOutput[Position]) => State(GameMap.eliminateNotExposedNonZeroCleanCells(Position2d.allAroundPositionF, Position2d.inF(size))))
            .andThen(
              seom.bind((_: FSMOutput[Position]) => State(GameMap.computeConnectedPositionSequence(Position2d.allAdjacentPositionF)))
            )
            .andThen(
              seom.bind(x => State(GameMap.computeAdjacentPositionPowerSet(Position2d.assertAdjacent)(x)))
            )
            .andThen(
              seom.bind(x => State(GameMap.tryToSolveThenPushToBeSteppedOn(Position2d.allAroundPositionF, Position2d.inF(size), Implicits.global)(x)))
            )(seom.unit(NothingToDo()))
        )

    val (resultGameState, resultFsmOutput) = composedF(size).f(gameState)

    Console.out.println(Printer2d.mapToString(resultGameState.gameMap.size, resultGameState.gameMap.map))
    resultFsmOutput match {
      case Right(Some(FoundAnswer(result))) =>
        result.map(_._2) should equal(List(Map((Position(0, 4) -> true)), Map((Position(4, 4) -> true))))
      case _ => fail()
    }
    resultGameState.beSteppedOnCell should equal(Queue())
    resultGameState.beSteppedOnCellLookup should equal(Set())

    val map2 =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size),
        Position2d.origin
      )(size, Map((Position(0, 4) -> MineCell), (Position(1, 4) -> MineCell), (Position(4, 4) -> MineCell)))
    val gameState2 =
      GameState(
        GameMap.GameMap(size, map2),
        Queue(Position(0, 0)),
        Set(Position(0, 0)),
        Set()
      )
    val (resultGameState2, resultFsmOutput2) = composedF(size).f(gameState2)
    Console.out.println(Printer2d.mapToString(resultGameState2.gameMap.size, resultGameState2.gameMap.map))
    resultFsmOutput2 match {
      case Right(Some(FoundAnswer(result))) =>
        result.map(_._2) should equal(List(Map((Position(0, 4) -> true), (Position(1, 4) -> true), (Position(2, 4) -> false), (Position(3, 4) -> false), (Position(4, 4) -> true))))
      case _ => fail()
    }
    resultGameState2.beSteppedOnCell should equal(Queue(Position(3, 4), Position(2, 4)))
    resultGameState2.beSteppedOnCellLookup should equal(Set(Position(2, 4), Position(3, 4)))

    // 부분집합을 만드는 방법을 바꾸면서, 순서도 바뀌었다. 위 단계에서 답 5개를 한꺼번에 전부 찾고 아래에서 더 이상 찾을 것이 없다.
    val (resultGameState3, resultFsmOutput3) = composedF(size).f(resultGameState2)
    Console.out.println(Printer2d.mapToString(resultGameState3.gameMap.size, resultGameState3.gameMap.map))
    resultFsmOutput3 match {
      case Right(None) =>
      case _ => fail()
    }

    // 한 개의 답을 찾을 수 없는 경우, 가능한 모든 경우를 보이는 출력을 해야 한다.
    val size2 = Position(3, 3)
    val map3 =
      GameMap.arrangeMap(
        Position2d.allAroundPositionF,
        Position2d.inF(size2),
        Position2d.origin
      )(size, Map((Position(0, 0) -> MineCell), (Position(2, 2) -> MineCell)))
    val gameState3 =
      GameState(
        GameMap.GameMap(size2, map3),
        Queue(Position(0, 2)),
        Set(Position(0, 2)),
        Set()
      )
    val (resultGameState4, resultFsmOutput4) = composedF(size2).f(gameState3)
    Console.out.println(Printer2d.mapToString(resultGameState4.gameMap.size, resultGameState4.gameMap.map))
    resultFsmOutput4 match {
      case Right(Some(NotFoundAnswer(_))) =>
      case _ => fail()
    }
  }

  test("findUnknownCellAroundCompleted") {
    def composedF(size: Position) =
      seom.orElse[FSMOutput[Position]](
        (_, b, _) => b
      )(composedRepeat(size))(
          seom.bind((_: FSMOutput[Position]) => State(GameMap.eliminateNotExposedNonZeroCleanCells(Position2d.allAroundPositionF, Position2d.inF(size))))
            .andThen(
              seom.bind((_: FSMOutput[Position]) => State(GameMap.computeConnectedPositionSequence(Position2d.allAdjacentPositionF)))
            )
            .andThen(
              seom.bind(x => State(GameMap.computeAdjacentPositionPowerSet(Position2d.assertAdjacent)(x)))
            )(seom.unit(NothingToDo()))
        )

    val size3X2 = Position(3, 2)

    def template(size: Position, mines: Stream[Position], beSteppedOn: Stream[Position]): (GameState[Position], Stream[Stream[Map[Position, Boolean]]]) = {
      val map =
        GameMap.arrangeMap(
          Position2d.allAroundPositionF,
          Position2d.inF(size),
          Position2d.origin
        )(size, mines.map(p => (p, MineCell)).toMap)
      val gameState =
        GameState(
          GameMap.GameMap(size, map),
          Queue(beSteppedOn: _*),
          Set(beSteppedOn: _*),
          Set()
        )
      val (resultGameState, resultFsmOutput) = composedF(size).f(gameState)
      Console.out.println(Printer2d.mapToString(resultGameState.gameMap.size, resultGameState.gameMap.map))
      val psss =
        resultFsmOutput match {
          case Right(Some(AdjacentPositionPowerSet(psss))) => psss
        }
      val result =
        psss
          //          .flatMap(_.toList)
          .flatten
          .map(GameMap.solve(Position2d.allAroundPositionF, Position2d.inF(size))(resultGameState.gameMap.size, resultGameState.gameMap.map)(Stream((Stream.Empty, Map[Position, Boolean]()))))
          .map(_.map(_._2).distinct)
          .filterNot(_.filterNot(_.isEmpty).isEmpty)
          .distinct
      Console.out.println(result.mkString("\n"))
      (resultGameState, result)
    }

    val (resultGameState3X2_1, result3X2_1) =
      template(
        size3X2,
        Stream(Position(1, 1)),
        Stream(Position(0, 0), Position(1, 0), Position(2, 0))
      )

    val modifiedMap = resultGameState3X2_1.gameMap.map + (Position(1, 1) -> ExpectedAsMineCell(MineCell))
    val expectedUnknownCell = Set(Position(0, 1), Position(2, 1))
    // 이 검사의 요지는, 깃발을 모두 꽂은 뒤, 밟을 수 있는 칸을 제대로 찾는지 확인하는 것이다.
    // 예를 들어,
    // . . .
    // . M . 이고
    // 1 1 1
    // ? E ? 이라고 깃발을 꽂았을 때,
    // 첫번째 1 주위에 완료된 칸은 첫번째 1 과 두번째 1 이고 첫번째 1 주위의 빈칸은 첫번째 ?, 두번째 1 주위의 빈칸은 두번째 ? 이다.
    // 두번째 1 주위에 완료된 칸은 첫번째 1 과 세번째 1 이고 첫번째 1 주위의 빈칸은 첫번째 ?, 세번째 1 주위의 빈칸은 두번째 ? 이다.
    // 세번째 1 주위에 완료된 칸은 두번째 1 과 세번째 1 이고 두번째 1 주위의 빈칸은 첫번째 ?, 세번째 1 주위의 빈칸은 두번째 ? 이다.
    // 결론은, 이 검사에서는 모두 같은 답을 구한다.
    GameMap.findUnknownCellAroundCompleted(Position2d.allAroundPositionF, Position2d.inF(size3X2))(
      Position(0, 0), Queue(), Set(), modifiedMap
    )._2 should equal(expectedUnknownCell)
    GameMap.findUnknownCellAroundCompleted(Position2d.allAroundPositionF, Position2d.inF(size3X2))(
      Position(1, 0), Queue(), Set(), modifiedMap
    )._2 should equal(expectedUnknownCell)
    GameMap.findUnknownCellAroundCompleted(Position2d.allAroundPositionF, Position2d.inF(size3X2))(
      Position(2, 0), Queue(), Set(), modifiedMap
    )._2 should equal(expectedUnknownCell)
  }
}
