/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.minesweepersolver.common.combinator

import org.scalatest.{ Matchers, FunSuite }

/**
 * Created by Kang Woo, Lee on 9/21/16.
 */
class TestCombinator extends FunSuite with Matchers {
  test("unorderedUniqueCombinator") {
    // 순서없고 중복없는 조합이다.
    // 123 과 321 은 같고, 001 은 있을 수 없다.
    // powerSet/unorderedUniqueCombinator/orderedUniqueCombinator 조합에는 검사 규칙이 있다.
    // 여기서 확인한 조건이 규칙 전부는 아니다.
    // 일단, 모든 원소가 나타나는 회수가 같다.
    // 0, 1, 2
    // 0, 1, 3
    // 0, 2, 3
    // 1, 2, 3
    val result = Combinator.unorderedUniqueCombinator(Stream(None, None, None))(Stream(0, 1, 2, 3))
    result
      .foldLeft(Map[Int, Int]()) {
        case (map, xs) =>
          xs.foldLeft(map) {
            case (map, x) =>
              map.get(x) match {
                case Some(n) => map + (x -> (n + 1))
                case None => map + (x -> 1)
              }
          }
      } should equal(Map((0 -> 3), (1 -> 3), (2 -> 3), (3 -> 3)))
    result.toSet.size should equal(result.size)
    Console.out.println(result)

    // 0, 1, 2
    // 0, 1, 3
    // 0, 2, 3
    val result2 = Combinator.unorderedUniqueCombinator(Stream(Some(0), None, None))(Stream(1, 2, 3))
    result2
      .foldLeft(Map[Int, Int]()) {
        case (map, xs) =>
          xs.foldLeft(map) {
            case (map, x) =>
              map.get(x) match {
                case Some(n) => map + (x -> (n + 1))
                case None => map + (x -> 1)
              }
          }
      } should equal(Map((0 -> 3), (1 -> 2), (2 -> 2), (3 -> 2)))
    result2.toSet.size should equal(result2.size)
    Console.out.println(result2)
  }

  test("orderedUniqueCombinator") {
    // 순서있고 중복없는 조합이다. 001 은 있을 수 없다.
    // 2, 1, 0
    // 3, 1, 0
    // 1, 2, 0
    // 3, 2, 0
    // 1, 3, 0
    // 2, 3, 0
    // 2, 0, 1
    // 3, 0, 1
    // 0, 2, 1
    // 3, 2, 1
    // 0, 3, 1
    // 2, 3, 1
    // 1, 0, 2
    // 3, 0, 2
    // 0, 1, 2
    // 3, 1, 2
    // 0, 3, 2
    // 1, 3, 2
    // 1, 0, 3
    // 2, 0, 3
    // 0, 1, 3
    // 2, 1, 3
    // 0, 2, 3
    // 1, 2, 3
    val result = Combinator.orderedUniqueCombinator(Stream(None, None, None))(Stream(0, 1, 2, 3))
    result
      .foldLeft(Map[Int, Int]()) {
        case (map, xs) =>
          xs.foldLeft(map) {
            case (map, x) =>
              map.get(x) match {
                case Some(n) => map + (x -> (n + 1))
                case None => map + (x -> 1)
              }
          }
      } should equal(Map((0 -> 18), (1 -> 18), (2 -> 18), (3 -> 18)))
    result.toSet.size should equal(result.size)
    Console.out.println(result)
    // 0, 2, 1
    // 0, 3, 1
    // 0, 1, 2
    // 0, 3, 2
    // 0, 1, 3
    // 0, 2, 3
    val result2 = Combinator.orderedUniqueCombinator(Stream(Some(0), None, None))(Stream(1, 2, 3))
    result2
      .foldLeft(Map[Int, Int]()) {
        case (map, xs) =>
          xs.foldLeft(map) {
            case (map, x) =>
              map.get(x) match {
                case Some(n) => map + (x -> (n + 1))
                case None => map + (x -> 1)
              }
          }
      } should equal(Map((0 -> 6), (1 -> 4), (2 -> 4), (3 -> 4)))
    result2.toSet.size should equal(result2.size)
    Console.out.println(result2)

    // 동일성을 기준으로 같은 값이어도 xs 에서 나타나는 순서가 다르면 다른 조합후보로 다룬다는 것을 검사한다.
    // 첫번째 true 와 두번째 true 는 같은 값이지만 다른 조합 후보라고 본다.
    val result3 = Combinator.orderedUniqueCombinator(Stream(None, None))(Stream(true, true))
    result3 should equal(Stream(Stream(true, true), Stream(true, true)))
  }

  test("orderedDuplicatedCombinator") {
    // 순서있고 중복있는 조합이다. 001 과 100 은 다르다.
    // 0, 0, 0
    // 1, 0, 0
    // 2, 0, 0
    // 0, 1, 0
    // 1, 1, 0
    // 2, 1, 0
    // 0, 2, 0
    // 1, 2, 0
    // 2, 2, 0
    // 0, 0, 1
    // 1, 0, 1
    // 2, 0, 1
    // 0, 1, 1
    // 1, 1, 1
    // 2, 1, 1
    // 0, 2, 1
    // 1, 2, 1
    // 2, 2, 1
    // 0, 0, 2
    // 1, 0, 2
    // 2, 0, 2
    // 0, 1, 2
    // 1, 1, 2
    // 2, 1, 2
    // 0, 2, 2
    // 1, 2, 2
    // 2, 2, 2
    val result = Combinator.orderedDuplicatedCombinator(Stream(Left(0), Left(1), Left(2)))
    result
      .foldLeft(Map[Int, Int]()) {
        case (map, xs) =>
          xs.foldLeft(map) {
            case (map, x) =>
              map.get(x) match {
                case Some(n) => map + (x -> (n + 1))
                case None => map + (x -> 1)
              }
          }
      } should equal(Map((0 -> 27), (1 -> 27), (2 -> 27)))
    result.toSet.size should equal(result.size)
    Console.out.println(result)
    // 0, 1, 1, 1
    // 0, 2, 1, 1
    // 0, 3, 1, 1
    // 0, 1, 2, 1
    // 0, 2, 2, 1
    // 0, 3, 2, 1
    // 0, 1, 3, 1
    // 0, 2, 3, 1
    // 0, 3, 3, 1
    // 0, 1, 1, 2
    // 0, 2, 1, 2
    // 0, 3, 1, 2
    // 0, 1, 2, 2
    // 0, 2, 2, 2
    // 0, 3, 2, 2
    // 0, 1, 3, 2
    // 0, 2, 3, 2
    // 0, 3, 3, 2
    // 0, 1, 1, 3
    // 0, 2, 1, 3
    // 0, 3, 1, 3
    // 0, 1, 2, 3
    // 0, 2, 2, 3
    // 0, 3, 2, 3
    // 0, 1, 3, 3
    // 0, 2, 3, 3
    // 0, 3, 3, 3
    val result2 = Combinator.orderedDuplicatedCombinator(Stream(Right(0), Left(1), Left(2), Left(3)))
    result2
      .foldLeft(Map[Int, Int]()) {
        case (map, xs) =>
          xs.foldLeft(map) {
            case (map, x) =>
              map.get(x) match {
                case Some(n) => map + (x -> (n + 1))
                case None => map + (x -> 1)
              }
          }
      } should equal(Map((0 -> 27), (1 -> 27), (2 -> 27), (3 -> 27)))
    result2.toSet.size should equal(result2.size)
    Console.out.println(result2)
  }

  test("adjacentPowerSet") {
    // 이 기능은 아래와 같은 부분집합을 구할 때 쓴다.
    // ()
    // 0
    // 1
    // 0, 1
    // 2
    // 0, 1, 2
    // 1, 2
    // 3
    // 1, 2, 3
    // 0, 1, 2, 3
    // 2, 3
    // 4
    // 2, 3, 4
    // 0, 1, 2, 3, 4
    // 1, 2, 3, 4
    // 3, 4
    val xs = Stream(0, 1, 2, 3, 4)
    def predF(a: Int, b: Int): Boolean = {
      val c = a - b
      (c == 1 || c == -1)
    }
    val powerSet = Combinator.adjacentPowerSet(predF)(xs)
    val result =
      powerSet
        .foldLeft(Map[Int, Int]()) {
          case (map, xs) =>
            xs.foldLeft(map) {
              case (map, x) =>
                map.get(x) match {
                  case Some(n) => map + (x -> (n + 1))
                  case None => map + (x -> 1)
                }
            }
        }
    val expected = Map((0 -> 5), (1 -> 8), (2 -> 9), (3 -> 8), (4 -> 5))
    result should equal(expected)
    powerSet.toSet.size should equal(powerSet.size)
    Console.out.println(powerSet)
  }
}
