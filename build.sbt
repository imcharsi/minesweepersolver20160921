import de.heikoseeberger.sbtheader.license.Apache2_0

name := "MineSweeperSolver20160921"

val rxJavaVersion: SettingKey[String] = SettingKey("RxJavaVersion")
val scalatestVersion: SettingKey[String] = SettingKey("ScalatestVersion")

val commonSettings =
  Seq(
    version := "0.1-SNAPSHOT",
    scalaVersion := "2.11.8",
    rxJavaVersion := "1.1.9",
    scalatestVersion := "3.0.0",
    libraryDependencies += "org.scala-lang" % "scala-library" % scalaVersion.value,
    libraryDependencies += "io.reactivex" % "rxjava" % rxJavaVersion.value,
    libraryDependencies += "io.reactivex" %% "rxscala" % "0.26.2",
    libraryDependencies += "org.scalatest" %% "scalatest" % scalatestVersion.value % Test,
    headers :=
      Map(
        "scala" -> Apache2_0("2016", "KangWoo, Lee (imcharsi@hotmail.com)")
      )
  )
val common =
  (project in file("common"))
    .settings(commonSettings)
    .enablePlugins(AutomateHeaderPlugin)

val clientSettings =
  Seq(
    libraryDependencies += "io.reactivex" % "rxjavafx" % "0.1.4",
    fork in run := true
  )
val client =
  (project in file("client"))
    .settings(commonSettings)
    .settings(clientSettings)
    .dependsOn(common % "compile->compile")
    .dependsOn(common % "test->test")
    .enablePlugins(AutomateHeaderPlugin)

val root =
  (project in file("."))
    .settings(commonSettings)
    .aggregate(common, client)